extends PanelContainer



func _on_Quit_pressed():
	$SFX/SelectOption.play()
	yield($SFX/SelectOption, "finished")
	get_tree().quit()

func _on_StartGame_pressed():
	$SFX/SelectOption.play()
	get_parent().get_parent().load_stage()

func _on_Options_option_hover():
	$SFX/HighlightOption.play()


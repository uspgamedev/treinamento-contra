extends VBoxContainer

signal option_hover

# Called when the node enters the scene tree for the first time.
func _ready():
	for button in self.get_children():
		button.connect("mouse_entered", self, "_on_mouse_entered")

func _on_mouse_entered():
	emit_signal("option_hover")
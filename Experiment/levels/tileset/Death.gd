extends Area2D





func _on_body_entered(body):
	if body.is_in_group("player"):
		body.take_damage(100.0, Vector2.ZERO)
	elif body.is_in_group("enemy"):
		body.die()

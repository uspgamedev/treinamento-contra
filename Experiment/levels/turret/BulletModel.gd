extends Sprite

export (PackedScene) var bullet
export (PackedScene) var muzzle_flash_particles
export (PackedScene) var muzzle_flash_light
export (float, 100) var barrel_size

export (Dictionary) var idle_frame = {
	"Forward": 0,
	"Diagonal": 0,
	"Upwards": 0
}

onready var turret: Node = get_parent()
onready var level: Node = turret.get_node("Finder").get("stage")
onready var player: AnimationPlayer = $AnimationPlayer
onready var timer: Timer = turret.get_node("Cooldown")



func spawn_muzzle_flash(x: float, y: float, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var flash = muzzle_flash_particles.instance()
	var muzzle = turret.muzzle[turret.model][turret.pointing]
	flash.emission_rect_extents = Vector2(x,y)
	flash.lifetime = time
	flash.amount = 72*4*x*y/1024
	flash.position = Vector2(x, 0) + offset
	muzzle.add_child(flash)

func spawn_muzzle_flash_light(scale: float = 1.0, energy: float = 1.0, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var light = muzzle_flash_light.instance()
	var muzzle = turret.muzzle[turret.model][turret.pointing]
	light.texture_scale = scale
	light.init_energy = energy
	light.lifetime = time
	light.position = Vector2(0, 0) + offset
	muzzle.add_child(light)

func fire(direction: Vector2) -> void:
	if can_shoot():
		var shot = bullet.instance()
		var muzzle = turret.muzzle[turret.model][turret.pointing]
		shot.position = turret.position
		shot.position += lin_transf(muzzle.position, turret.transform.x, turret.transform.y)
		shot.position += direction.normalized()*barrel_size
		shot.shoot_direction = direction.normalized()
		level.add_child(shot)
		
		spawn_muzzle_flash_light()
		spawn_muzzle_flash(4, 10)
		spawn_muzzle_flash(6, 14, Vector2(8,0))
		spawn_muzzle_flash(6, 8, Vector2(20,0))
		spawn_muzzle_flash(6, 16, Vector2(32,0))
		spawn_muzzle_flash(6, 8, Vector2(44,0))
		spawn_muzzle_flash(4, 16, Vector2(56,0))
		spawn_muzzle_flash(4, 10, Vector2(62,0))
		
		timer.start(turret.fire_cooldown)
		player.play(turret.pointing)

func can_shoot() -> bool:
	return timer.is_stopped() and not player.is_playing()

# This function transforms vector vec, on the canonical base, to itself on the basis
# (basis_x, basis_y).
func lin_transf(vec: Vector2, basis_x: Vector2, basis_y: Vector2) -> Vector2:
	var prod: Vector2
	prod = Vector2(vec.x*basis_x.x + vec.y*basis_y.x, vec.x*basis_x.y + vec.y*basis_y.y)
	return prod




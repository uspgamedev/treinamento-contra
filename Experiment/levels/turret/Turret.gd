extends Node2D
class_name Turret

export (float, 2000) var vision_range
export (float, 2) var fire_cooldown

export (String, "Bullet", "Missile", "Shell") var model = "Bullet" setget set_model
export (String, "Forward", "Diagonal", "Upwards") var pointing = "Forward" setget set_pointing
export (bool) var flip_h = false setget set_flip_h
export (bool) var flip_v = false setget set_flip_v

onready var active: bool = false
onready var player: Node = $Finder.get("player")

onready var dir: Dictionary = {
	"Forward": Vector2(-1,0),
	"Diagonal": Vector2(-1,-1),
	"Upwards": Vector2(0,-1)
}

onready var gun: Dictionary = {
	"Bullet": $BulletModel,
	"Missile": $MissileModel,
	"Shell": $ShellModel
}

onready var hitbox: Dictionary = {
	"Bullet": {
		"Forward": get_node("BulletModel/ForwardHitbox/CollisionShape2D"),
		"Diagonal": get_node("BulletModel/DiagonalHitbox/CollisionShape2D"),
		"Upwards": get_node("BulletModel/UpwardsHitbox/CollisionShape2D")
	},
	
	"Missile": {
		"Forward": get_node("MissileModel/ForwardHitbox/CollisionShape2D"),
		"Diagonal": get_node("MissileModel/DiagonalHitbox/CollisionShape2D"),
		"Upwards": get_node("MissileModel/UpwardsHitbox/CollisionShape2D")
	},
	
	"Shell": {
		"Forward": get_node("ShellModel/ForwardHitbox/CollisionShape2D"),
		"Diagonal": get_node("ShellModel/DiagonalHitbox/CollisionShape2D"),
		"Upwards": get_node("ShellModel/UpwardsHitbox/CollisionShape2D")
	}
}

onready var muzzle: Dictionary = {
	"Bullet": {
		"Forward": get_node("BulletModel/ForwardHitbox/MuzzlePosition"),
		"Diagonal": get_node("BulletModel/DiagonalHitbox/MuzzlePosition"),
		"Upwards": get_node("BulletModel/UpwardsHitbox/MuzzlePosition")
	},
	
	"Missile": {
		"Forward": get_node("MissileModel/ForwardHitbox/MuzzlePosition"),
		"Diagonal": get_node("MissileModel/DiagonalHitbox/MuzzlePosition"),
		"Upwards": get_node("MissileModel/UpwardsHitbox/MuzzlePosition")
	},
	
	"Shell": {
		"Forward": get_node("ShellModel/ForwardHitbox/MuzzlePosition"),
		"Diagonal": get_node("ShellModel/DiagonalHitbox/MuzzlePosition"),
		"Upwards": get_node("ShellModel/UpwardsHitbox/MuzzlePosition")
	}
}


##### For the editor ######################################################
func set_model(new_value: String) -> void:
	model = new_value

func set_pointing(new_value: String) -> void:
	pointing = new_value

func set_flip_h(new_value: bool) -> void:
	flip_h = new_value
	if flip_h == true:
		transform.x.x = -1
	else:
		transform.x.x = 1

func set_flip_v(new_value: bool) -> void:
	flip_v = new_value	
	if flip_v == true:
		transform.y.y = -1
	else:
		transform.y.y = 1
	
###########################################################################


func _ready():
	if not Engine.editor_hint:
		hitbox[model][pointing].get_parent().visible = true
		hitbox[model][pointing].disabled = false

func _physics_process(_delta):
	if not Engine.editor_hint:
		if vision_range > 0:
			if (player.position - self.position).length() <= vision_range:
				activate()
			else:
				deactivate()
		if is_active():
			gun[model].fire(Vector2(dir[pointing].x*transform.x.x,
									dir[pointing].y*transform.y.y))

func is_active() -> bool:
	return active

func activate() -> void:
	active = true

func deactivate() -> void:
	active = false

func on_off_trigger() -> void:
	if active:
		deactivate()
	else:
		activate()





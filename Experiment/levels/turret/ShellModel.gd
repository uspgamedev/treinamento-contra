extends Sprite

export (PackedScene) var shell
export (PackedScene) var smoke_particles
export (float, 100) var barrel_size

export (Dictionary) var idle_frame = {
	"Forward": 0,
	"Diagonal": 0,
	"Upwards": 0
}

onready var turret: Node = get_parent()
onready var level: Node = turret.get_node("Finder").get("stage")
onready var player: AnimationPlayer = $AnimationPlayer
onready var timer: Timer = turret.get_node("Cooldown")


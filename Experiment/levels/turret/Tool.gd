tool
extends Turret



func set_model(new_value: String) -> void:
	model = new_value
	if ready or Engine.editor_hint:
		for key in gun.keys():
			gun[key].visible = false
		gun[model].visible = true
		gun[model].frame = gun[model].idle_frame[pointing]

func set_pointing(new_value: String) -> void:
	pointing = new_value
	if ready or Engine.editor_hint:
		gun[model].frame = gun[model].idle_frame[pointing]



var ready: bool

func _ready() -> void:
	ready = true
	set_model(model)
	set_pointing(pointing)
	set_flip_h(flip_h)
	set_flip_v(flip_v)






extends Sprite

export (PackedScene) var missile
export (PackedScene) var smoke_particles
export (float, 100) var barrel_size

export (Dictionary) var idle_frame = {
	"Forward": 0,
	"Diagonal": 0,
	"Upwards": 0
}

onready var turret: Node = get_parent()
onready var level: Node = turret.get_node("Finder").get("stage")
onready var player: AnimationPlayer = $AnimationPlayer
onready var timer: Timer = turret.get_node("Cooldown")



onready var just_shot: bool = false

func can_fire() -> bool:
	return not just_shot and timer.is_stopped() and not player.is_playing()

func fire(direction: Vector2) -> void:
	if can_fire():
		var shot = missile.instance()
		var muzzle = turret.muzzle[turret.model][turret.pointing]
		shot.position = turret.position
		shot.position += lin_transf(muzzle.position, turret.transform.x, turret.transform.y)
		shot.position += direction.normalized()*barrel_size
		shot.starting_direction = direction
		shot.connect("exploded", self, "on_missile_explode")
		level.add_child(shot)
		#muzzle.add_child(smoke_particles.instance())
		player.play(turret.pointing)

func on_missile_explode() -> void:
	just_shot = false
	timer.start(turret.fire_cooldown)

# This function transforms vector vec, on the canonical base, to itself on the basis
# (basis_x, basis_y).
func lin_transf(vec: Vector2, basis_x: Vector2, basis_y: Vector2) -> Vector2:
	var prod: Vector2
	prod = Vector2(vec.x*basis_x.x + vec.y*basis_y.x, vec.x*basis_x.y + vec.y*basis_y.y)
	return prod



extends Area2D
class_name CameraLimiter


var left_limit
var right_limit
var top_limit
var bottom_limit


func _ready() -> void:
	self.connect("body_entered", self, "on_player_entered")
	set_collision_layer(32)
	set_collision_mask(32)
	left_limit = $CollisionPolygon2D.polygon[0].x
	right_limit = $CollisionPolygon2D.polygon[0].x
	top_limit = $CollisionPolygon2D.polygon[0].y
	bottom_limit = $CollisionPolygon2D.polygon[0].y
	for point in $CollisionPolygon2D.polygon:
		if point.x > right_limit:
			right_limit = point.x
		if point.x < left_limit:
			left_limit = point.x
		if point.y < top_limit:
			top_limit = point.y
		if point.y > bottom_limit:
			bottom_limit = point.y



func on_player_entered(player) -> void:
	var camera: Camera2D = player.get_node("Camera")

	camera.new_bottom_limit = bottom_limit + position.y
	camera.new_left_limit = left_limit + position.x
	camera.new_top_limit = top_limit + position.y
	camera.new_right_limit = right_limit + position.x

	if camera.new_bottom_limit < player.position.y + 500:
		camera.limit_bottom = player.position.y + 500
	if camera.new_top_limit > player.position.y - 500:
		camera.limit_top = player.position.y - 500
	if camera.new_left_limit > player.position.x - 1000:
		camera.limit_left = player.position.x - 1000
	if camera.new_right_limit < player.position.x + 1000:
		camera.limit_right = player.position.x + 1000




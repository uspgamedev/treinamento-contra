extends Position2D
class_name EnemySpawn

export (Script) var enemy_list
export(bool) var one_shot
export(String, "Ground Enemy", "Flying Enemy") var enemy_name
export(String, "Horizontal", "Vertical", "Multidirectional") var aim_type
export(float) var vision_range
export(float) var enemy_vision_range
export(float) var time_between_spawn
export(float) var fire_cooldown
export(bool) var disabled
export(bool) var immobile
export (String, "Pistol", "Machine Gun", "Shotgun", "Rocket Launcher", "Grenade Launcher") var weapon

onready var has_spawn: bool = false
onready var bestiary = enemy_list.new()
onready var level = get_tree().get_nodes_in_group("stage")[0]

func _ready():
	if disabled:
		self.queue_free()

func spawn_enemy():
	var enemy = bestiary.enemy_dictionary[enemy_name].instance()
	enemy.position = self.position
	enemy.vision_range = enemy_vision_range
	enemy.aim_type = aim_type
	enemy.fire_cooldown = fire_cooldown
	enemy.weapon = weapon
	if immobile:
		enemy.max_horizontal_speed = 0.0

	enemy.connect("died", self, "_on_spawn_death")
	level.add_child(enemy)
	has_spawn = true

func activate():
	spawn_enemy()

func _on_spawn_timer_timeout():
	spawn_enemy()

func _on_spawn_death():
	has_spawn = false
	if !one_shot:
		$spawn_timer.start(time_between_spawn)
	else:
		self.queue_free()





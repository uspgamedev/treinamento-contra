extends BaseState

func enter():
	creature.activate()

func routine():
	if !player_in_range() and !creature.has_spawn:
		next_state = creature_ai.get_node("Waiting")
		exit()

func exit():
	creature.get_node("spawn_timer").stop()
	.exit()
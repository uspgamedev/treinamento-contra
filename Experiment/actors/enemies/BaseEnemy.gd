extends BaseCreature
class_name BaseEnemy

export(int) var contact_damage
export(float) var gravity_acceleration
export(float) var horizontal_acceleration
export(float) var max_horizontal_speed
export(float) var contact_knockback
export(bool) var is_invulnerable


onready var sprite = $Sprite
onready var tween = $Tween
onready var gun_sprite = get_node("Sprite/GunSprite")
onready var gun_texture = gun_sprite.get_node("Sprite")
onready var is_attacking: bool = false
onready var facing: float = -1.0
var vision_range: float
var fire_cooldown: float

# warning-ignore:unused_signal
signal died

func _process(delta):
	
	if move > 0:
		facing = 1.0
	elif move < 0:
		facing = -1.0
	
	if is_dead():
		self.die()
	
	deal_with_spikes()

func _ready():
	parse()

func attack(direction: Vector2) -> void:
	pass

func parse():
	self.gravity = gravity_acceleration
	self.acceleration = horizontal_acceleration
	self.max_natural_speed = max_horizontal_speed
	self.max_falling_speed = MAX_SPEED
	self.max_rising_speed = MAX_SPEED

func take_damage(damage_amount: int, source_direction = Vector2(), knockback_force:float = 0.0) -> void:
	if !is_invulnerable:
		.take_damage(damage_amount,Vector2(source_direction.x,0),knockback_force)
		if not self.is_dead():
			tween.interpolate_property(sprite, "modulate",
										Color(10,10,10,0.10), Color(1,1,1,1), 0.15,
										Tween.TRANS_QUAD, Tween.EASE_IN)
			
			#tween.interpolate_property(sprite.get_material(), "shader_param/color",
			#							Color(0.7,0.7,0.7,1), Color(0,0,0,1), 0.15,
			#							Tween.TRANS_QUAD, Tween.EASE_IN)
			tween.start()

func is_visible() -> bool:
	return $VisibilitySensor.is_on_screen()

func die() -> void:
	if $AI.states[0] != $AI.state("Dead"):
		$AI.states[0].next_state = $AI.state("Dead")
		$AI.states[0].exit()

func is_player_visible() -> void:
	pass

func attack_state() -> void:
	pass

func _on_HurtBox_body_entered(body):
	if body.is_in_group("player"):
		var damage_direction = body.position - self.position
		body.take_damage(contact_damage, damage_direction, contact_knockback)



func aim_gun(direction: Vector2) -> void:
	if direction.x < 0:
		gun_texture.flip_v = true
		gun_sprite.rotation_degrees = rad2deg(direction.angle())
	else:
		gun_texture.flip_v = false
		gun_sprite = rad2deg(direction.angle())

func rest_position() -> void:
	if self.facing > 0:
		gun_texture.flip_v = false
		gun_sprite.rotation_degrees = 0
	else:
		gun_texture.flip_v = true
		gun_sprite.rotation_degrees = 180



func deal_with_spikes() -> void:
	if is_touching_spikes():
		if is_touching_down_spikes():
			self.take_damage(100, Vector2(0,-1), 500)
		elif is_touching_left_spikes():
			self.take_damage(100, Vector2(1,-1), 700)
		elif is_touching_right_spikes():
			self.take_damage(100, Vector2(-1,-1), 700)
		elif is_touching_up_spikes():
			self.take_damage(100, Vector2(0,1), 0)


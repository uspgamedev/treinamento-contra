extends BaseEnemy

export(float) var flap_delay

onready var active: bool = false

func wing_flap() -> void:
	self.velocity.y *= -1

func on_flap_timer_timeout():
	wing_flap()

func _on_Down_body_exited(body):
	if body.is_in_group("floor") and just_jumped_out_of_floor(body):
		var flap_timer:= Timer.new()
		self.add_child(flap_timer)
		flap_timer.connect("timeout", self, "on_flap_timer_timeout")
		flap_timer.start(flap_delay)

func just_jumped_out_of_floor(exception: StaticBody2D = null) -> bool:
	for body in self.get_down_areas():
		if body.is_in_group("floor") and body != exception:
			return false
	return true

func _on_VisibilitySensor_screen_entered():
	if !active:
		var player_position = self.get_parent().get_node("Player").position
		move = (player_position - self.position).normalized().x

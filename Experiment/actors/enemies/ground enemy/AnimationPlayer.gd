extends AnimationControl

onready var enemy: Node = get_parent()
onready var sprite: Node = enemy.get_node("Sprite")



func _process(delta):
	
	if enemy.facing > 0:
		sprite.flip_h = false
	else:
		sprite.flip_h = true
	
	if not enemy.is_dead():
		if enemy.is_on_floor() and enemy.velocity.x == 0:
			play_animation("Idle")
		elif enemy.is_on_floor() and enemy.velocity.x!= 0:
			play_animation("Moving")
		elif enemy.is_airborne():
			play_animation("Airborne")
	else:
		play_animation("Dying")



func play_animation(anim: String) -> void:
	if current_animation != anim:
		play(anim)
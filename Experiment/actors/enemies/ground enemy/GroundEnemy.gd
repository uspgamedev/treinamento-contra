extends BaseEnemy

export (String, "Pistol", "Machine Gun", "Shotgun", "Rocket Launcher", "Grenade Launcher") var weapon

onready var aim_type: String = "Horizontal"
onready var aim_timer = get_node("Sprite/GunSprite/AimTimer")
onready var player = $Finder.get("player")

var gun: Node

func _ready():
	move = -1
	match weapon:
		"Pistol":
			gun_texture.frame = 0
			gun = get_node("Guns/Pistol")
		"Machine Gun":
			gun_texture.frame = 1
			gun = get_node("Guns/MachineGun")
		"Shotgun":
			gun_texture.frame = 2
			gun = get_node("Guns/Shotgun")
		"Rocket Launcher":
			gun_texture.frame = 3
			gun = get_node("Guns/RocketLauncher")
		"Grenade Launcher":
			gun_texture.frame = 4
			gun = get_node("Guns/GrenadeLauncher")

func _physics_process(delta):
	if aim_timer.is_stopped():
		if self.facing > 0:
			gun_sprite.rotation_degrees = 0
			gun_texture.flip_v = false
		else:
			gun_sprite.rotation_degrees = 180
			gun_texture.flip_v = true



func attack(direction: Vector2) -> void:
	match aim_type:
		"Multidirectional":
			gun.fire(direction)
		"Horizontal":
			gun.fire(Vector2(self.facing, 0))
		"Vertical":
			if player.position.y > self.position.y:
				gun.fire(Vector2(0, 1))
			else:
				gun.fire(Vector2(0, -1))




extends EnemyGun




export (int, 1, 5) var shots_fired

func fire(direction: Vector2) -> void:
	
	for i in range(shots_fired):
		var bullet = projectile.instance()
		bullet.position = creature.position + direction.normalized()*barrel_size*MOD
		bullet.position.y -= 40*i
		bullet.shoot_direction = direction
		creature.get_parent().add_child(bullet)
	
	spawn_muzzle_flash_light()
	spawn_muzzle_flash(4, 24, Vector2(0, -20*creature.facing))
	spawn_muzzle_flash(4, 52, Vector2(8, -44*creature.facing))
	spawn_muzzle_flash(8, 64, Vector2(16, -52*creature.facing))
	spawn_muzzle_flash(4, 52, Vector2(32, -52*creature.facing))
	
	creature.is_attacking = false
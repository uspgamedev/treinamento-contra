extends Node2D


export (PackedScene) var rocket
#export (PackedScene) var smoke
export (float, 1000) var rocket_speed
export (float) var angular_acceleration
export (float) var max_angular_speed
export (float, 40) var rocket_life_time
export (float, 128) var barrel_size

onready var creature: Node = get_parent().get_parent()
onready var creature_shoulder: Node = creature.get_node("Sprite/GunSprite")


func fire(direction: Vector2) -> void:
	var missile = rocket.instance()
	missile.position = creature.position + direction.normalized()*barrel_size
	missile.speed = rocket_speed
	missile.angular_acceleration = angular_acceleration
	missile.max_angular_speed = max_angular_speed
	missile.life_time = rocket_life_time
	missile.starting_direction = direction
	missile.connect("exploded", self, "on_rocket_explode")
	creature.get_parent().add_child(missile)


func on_rocket_explode() -> void:
	creature.is_attacking = false
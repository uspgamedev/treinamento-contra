extends EnemyGun



onready var burst: int = 0
onready var direction: Vector2 = Vector2.ZERO



func _physics_process(delta):
	
	if burst > 0 and $Timer.is_stopped():
		var bullet = projectile.instance()
		bullet.position = creature.position + direction.normalized()*barrel_size*MOD
		bullet.shoot_direction = direction
		creature.get_parent().add_child(bullet)
		$Timer.start()
		burst -= 1
		
		spawn_muzzle_flash_light()
		spawn_muzzle_flash(4, 10)
		spawn_muzzle_flash(6, 14, Vector2(8,0))
		spawn_muzzle_flash(6, 8, Vector2(20,0))
		spawn_muzzle_flash(6, 16, Vector2(32,0))
		spawn_muzzle_flash(6, 8, Vector2(44,0))
		spawn_muzzle_flash(4, 16, Vector2(56,0))
		spawn_muzzle_flash(4, 10, Vector2(62,0))
		
		if burst == 0:
			creature.is_attacking = false



func fire(target_direction: Vector2) -> void:
	burst = randi()%3 + 3
	direction = target_direction



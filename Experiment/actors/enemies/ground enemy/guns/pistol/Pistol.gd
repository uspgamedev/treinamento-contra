extends EnemyGun



func fire(direction: Vector2) -> void:
	var bullet = projectile.instance()
	bullet.position = creature.position + direction.normalized()*barrel_size*MOD
	bullet.shoot_direction = direction
	creature.get_parent().add_child(bullet)
	
	spawn_muzzle_flash_light()
	spawn_muzzle_flash(4, 6)
	spawn_muzzle_flash(4, 10, Vector2(8,0))
	spawn_muzzle_flash(8, 14, Vector2(16,0))
	spawn_muzzle_flash(4, 8, Vector2(32,0))
	
	creature.is_attacking = false
extends Node
class_name EnemyGun

const MOD: float = 1.4



export (PackedScene) var projectile
export (PackedScene) var muzzle_flash
export (PackedScene) var muzzle_flash_light
export (float, 2000) var projectile_speed
export (float, 128) var barrel_size

onready var creature: Node = get_parent().get_parent()
onready var gun: Node = creature.get_node("Sprite/GunSprite")



func spawn_muzzle_flash(x: float, y: float, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var flash = muzzle_flash.instance()
	flash.emission_rect_extents = Vector2(x,y)
	flash.lifetime = time
	flash.amount = 72*4*x*y/1024
	flash.position = Vector2(barrel_size + x, 0) + offset
	gun.add_child(flash)

func spawn_muzzle_flash_light(scale: float = 1.0, energy: float = 1.0, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var light = muzzle_flash_light.instance()
	light.texture_scale = scale
	light.init_energy = energy
	light.lifetime = time
	light.position = Vector2(barrel_size, 0) + offset
	gun.add_child(light)

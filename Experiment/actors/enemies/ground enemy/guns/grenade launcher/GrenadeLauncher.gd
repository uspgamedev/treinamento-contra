extends Node2D



export (PackedScene) var grenade
export (float) var grenade_gravity
export (float) var grenade_life_time
export (float) var launch_speed
export (float) var barrel_size

onready var creature: Node = get_parent().get_parent()
onready var creature_shoulder: Node = creature.get_node("Sprite/GunSprite")



func fire(direction: Vector2) -> void:
	
	if find_angle() == null and $ApproachTimer.is_stopped():
		var target: Vector2 = creature.get_node("Finder").get("player").position
		creature.move = target.x - creature.position.x
		$ApproachTimer.start()
	else:
		var bomb = grenade.instance()
		bomb.position = creature.position + direction.normalized()*barrel_size
		bomb.life_time = grenade_life_time
		bomb.gravity = grenade_gravity
		bomb.horizontal_damp = 0.0
		bomb.max_falling_speed = bomb.MAX_SPEED
		bomb.produce_smoke = true
		bomb.turn_sprite = true
		bomb.initial_velocity = Vector2(creature.facing, -find_angle()).normalized()*launch_speed
		bomb.connect("exploded", self, "on_grenade_explode")
		creature.get_parent().add_child(bomb)

func _on_ApproachTimer_timeout():
	creature.move = 0.0
	fire(Vector2(creature.facing,0))

func on_grenade_explode() -> void:
	creature.is_attacking = false



# This method finds out the angle in which the grenade has to be launched to hit the target, given
# a fixed initial speed v, gravity g and target location (x,y). If this angle's tanget is s,
# then it is the solution of the following equation:
#
#                       (gx²/v²)s² - (2x)s + (gx²/v² - 2y) = 0
#
# If it has no solutions, it means that the player is too far to be reached with the current launch
# speed, so the enemy needs to approach the player to get within reach of its weapon.
func find_angle():
	var target: Vector2 = creature.get_node("Finder").get("player").position
	target -= creature.position
	if target.x < 0:
		target.x *= -1
	
	var a = grenade_gravity*target.x*target.x/(launch_speed*launch_speed)
	var b = -2*target.x
	var c = a - 2*target.y
	
	if solve(a,b,c) == null:
		return null
	else:
		return solve(a,b,c).y


# This method solves the equation ax²+bx+c=0 for x. If it has no solutions, it returns null.
# If it has solutions, it returns a Vector2 with x as its smallest root and y its greatest root.
func solve(a: float, b: float, c: float):
	var delta = b*b - 4*a*c
	if delta < 0:
		return null
	else:
		return Vector2( (-b - sqrt(delta))/(2*a) , (-b + sqrt(delta))/(2*a) )







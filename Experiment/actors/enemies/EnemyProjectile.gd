extends BaseProjectile

export (PackedScene) var destroy_animation



func destroy() -> void:
	var fragments = destroy_animation.instance()
	fragments.position = self.position
	get_parent().add_child(fragments)
	queue_free()
extends BaseState

func enter():
	if creature.is_on_left_floor_ledge():
		creature.move = 1.0
	elif creature.is_on_right_floor_ledge():
		creature.move = -1.0
	else:
		creature.move = creature.facing

func routine():
	
	if player_in_range():
		next_state = creature_ai.state("AttackWindUp")
		creature.move = 0.0
		exit()
	
	else:
		if creature.is_on_left_floor_ledge():
			creature.move = 1.0
		elif creature.is_on_right_floor_ledge():
			creature.move = -1.0
		else:
			creature.move = creature.facing
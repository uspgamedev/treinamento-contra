extends BaseState


func enter():
	var direction = (self.locator.get("player").position - 
			creature.position).normalized()
	creature.attack(direction)

func routine():
	if !is_attacking():
		if player_in_range():
			self.next_state = creature_ai.get_node("AttackWindUp")
		else:
			self.next_state = creature_ai.get_node("Waiting")
		exit()

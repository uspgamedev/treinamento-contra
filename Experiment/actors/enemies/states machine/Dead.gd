extends BaseState

onready var tween: Tween = creature.get_node("Tween")
onready var sprite: Sprite = creature.get_node("Sprite")

func enter() -> void:
	creature.weight /= 4
	creature.force += Vector2(0, -100)
	creature.get_node("HurtBox").monitoring = false
	creature.set_collision_layer(8)
	creature.move = 0.0
	tween.interpolate_property(sprite, "modulate",
								Color(1,1,1,1), Color(1,1,1,0), 2*creature.control,
								Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	creature.queue_free()
	creature.emit_signal("died")



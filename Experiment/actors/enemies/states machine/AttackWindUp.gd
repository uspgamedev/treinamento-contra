extends BaseState

func enter():
	creature.is_attacking = true
	next_state = creature_ai.state("Attack")
	$Timer.start(creature.fire_cooldown)

func routine():
	var target = player.position - creature.position
	if target.x > 0:
		creature.facing = 1.0
	else:
		creature.facing = -1.0


func _on_Timer_timeout():
	exit()

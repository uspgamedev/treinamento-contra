extends KinematicBody2D
class_name BaseCreature

const MAX_SPEED: float = 3000.0
const DEACCELERATION: float = 1200.0

const LEFT_FLOOR = Vector2(-1,1)
const RIGHT_FLOOR = Vector2(1,1)
const LEFT_WALL = Vector2(-1,-1)
const RIGHT_WALL = Vector2(1,-1)

onready var main = $Finder.get("main")



## PHYSICS SYSTEM ##

# Speed control value. Used to quickly speed up or slow down the game.
var control: float

# Main variables. Their names are say it all. Store each body's true max speed, gravity and etc on an export on their
# own scene and initialize the variables here on the ready function as equal to the exports, so we can change the values
# of the variables here (the ones actually used for physics) without losing the initial value. For example, we can slow
# down creatures, invert gravity and other things on specific characters without losing the starting value, which is
# stored on the export on its scene.
var gravity: float
var acceleration: float
var max_natural_speed: float
var max_falling_speed: float
var max_rising_speed: float
var jump_force: float
var weight: float = 1.0

# Some auxiliary variables, used to compute other things.
var move: float
var velocity := Vector2()
var force := Vector2()
var true_velocity := Vector2()


func _physics_process(delta):
	control = main.control
	
	vertical_movement(delta)
	horizontal_movement(delta)
	
	# To prevent bugs, let's cap all speed to a set value equal to the constant MAX_SPEED.
	true_velocity = velocity + force/weight
	if true_velocity.length() > MAX_SPEED:
		true_velocity *= MAX_SPEED / velocity.length()
	
	true_velocity = move_and_slide(true_velocity*control, Vector2(0,-1))
	velocity = (true_velocity - force/weight)/control
	
	damping(delta)

func horizontal_movement(delta) -> void:
	if move != 0:
		move = move/abs(move)
	if velocity.x*move <= 0:
		velocity.x = 0
	velocity.x += acceleration*delta*move*control
	velocity.x = clamp(velocity.x, -max_natural_speed, max_natural_speed)

func vertical_movement(delta) -> void:
	velocity.y += gravity*delta*control
	velocity.y = clamp(velocity.y, -max_rising_speed, max_falling_speed)

func damping(delta) -> void:
	if force.length() > 0:
		var force_norm = force.length()
		force_norm -= DEACCELERATION*delta*control
		force_norm = clamp(force_norm, 0, MAX_SPEED)
		force *= force_norm/force.length()

func drop_off_of_platform() -> void:
	self.set_collision_layer_bit(2, false)
	self.set_collision_mask_bit(2, false)

func on_platform_exited(body):
	if body.is_in_group("platform"):
		self.set_collision_layer_bit(2, true)
		self.set_collision_mask_bit(2, true)



## COMBAT ##
export(int) var hit_points
var shoot_distance_from_origin := Vector2()
onready var current_hp: int = hit_points

func is_dead() -> bool:
	return current_hp <= 0

func take_damage(damage_amount: int, source_direction = Vector2(), knockback_force: float = 0.0) -> void:
	current_hp -= damage_amount
	self.force += source_direction.normalized()*knockback_force



## GET ADJACENT OBJECTS ##
func get_down_bodies():
	return get_node("Detectors/Down").get_overlapping_bodies()

func get_down_areas():
	return get_node("Detectors/Down").get_overlapping_areas()

func get_left_bodies():
	return get_node("Detectors/Left").get_overlapping_bodies()

func get_left_areas():
	return get_node("Detectors/Left").get_overlapping_areas()

func get_right_bodies():
	return get_node("Detectors/Right").get_overlapping_bodies()

func get_right_areas():
	return get_node("Detectors/Right").get_overlapping_areas()

func get_up_bodies():
	return get_node("Detectors/Up").get_overlapping_bodies()

func get_up_areas():
	return get_node("Detectors/Up").get_overlapping_areas()

func get_middle_bodies():
	return get_node("Detectors/Middle").get_overlapping_bodies()

func get_middle_areas():
	return get_node("Detectors/Middle").get_overlapping_areas()

func get_top_left_bodies():
	return get_node("Detectors/LeftWallLedge").get_overlapping_bodies()

func get_top_left_areas():
	return get_node("Detectors/LeftWallLedge").get_overlapping_areas()

func get_top_right_bodies():
	return get_node("Detectors/RightWallLedge").get_overlapping_bodies()

func get_top_right_areas():
	return get_node("Detectors/RightWallLedge").get_overlapping_areas()

func get_bottom_left_bodies():
	return get_node("Detectors/LeftFloorLedge").get_overlapping_bodies()

func get_bottom_left_areas():
	return get_node("Detectors/LeftFloorLedge").get_overlapping_areas()

func get_bottom_right_bodies():
	return get_node("Detectors/RightFloorLedge").get_overlapping_bodies()

func get_bottom_right_areas():
	return get_node("Detectors/RightFloorLedge").get_overlapping_areas()



# Methods to check if the creature is on the floor, touching a wall, etc.
func is_on_floor() -> bool:
	for body in get_down_bodies():
		if body.is_in_group("floor"):
			return true
	return false

func is_on_platform() -> bool:
	for body in get_down_bodies():
		if body.is_in_group("platform"):
			return true
	return false

func is_on_left_wall() -> bool:
	for body in get_left_bodies():
		if body.is_in_group("wall"):
			return true
	return false

func is_on_right_wall() -> bool:
	for body in get_right_bodies():
		if body.is_in_group("wall"):
			return true
	return false

func is_on_wall() -> bool:
	return is_on_left_wall() or is_on_right_wall()

func is_on_left_block() -> bool:
	for body in get_left_bodies():
		if body.is_in_group("block"):
			return true
	return false

func is_on_right_block() -> bool:
	for body in get_right_bodies():
		if body.is_in_group("block"):
			return true
	return false

func is_airborne() -> bool:
	for body in get_down_bodies():
		if body.is_in_group("block") or body.is_in_group("floor"):
			return false
	for body in get_left_bodies():
		if body.is_in_group("block"):
			return false
	for body in get_right_bodies():
		if body.is_in_group("block"):
			return false
	return true

##### Auxiliary methods to detect ledges ####
func left_floor_ledge() -> bool:
	for body in get_bottom_left_bodies():
		if body.is_in_group("floor"):
			return true
	return false

func right_floor_ledge() -> bool:
	for body in get_bottom_right_bodies():
		if body.is_in_group("floor"):
			return true
	return false

func left_wall_ledge() -> bool:
	for body in get_top_left_bodies():
		if body.is_in_group("block"):
			return true
	return false

func right_wall_ledge() -> bool:
	for body in get_top_right_bodies():
		if body.is_in_group("block"):
			return true
	return false
#############################################

func is_on_left_floor_ledge() -> bool:
	return is_on_floor() and not left_floor_ledge()

func is_on_right_floor_ledge() -> bool:
	return is_on_floor() and not right_floor_ledge()

func is_on_left_wall_ledge() -> bool:
	return is_on_left_block() and not left_wall_ledge()

func is_on_right_wall_ledge() -> bool:
	return is_on_right_block() and not right_wall_ledge()

func is_on_floor_ledge() -> bool:
	return is_on_left_floor_ledge() or is_on_right_floor_ledge()

func is_on_wall_ledge() -> bool:
	return is_on_left_wall_ledge() or is_on_right_wall_ledge()

func is_on_ledge() -> bool:
	return is_on_floor_ledge() or is_on_wall_ledge()

func get_ledge_distance(ledge):
	if not is_on_ledge():
		return 0
	else:
		var raycast = preload("res://common/Raycast.tscn").instance()
		raycast.set_enabled(true)
		raycast.set_collision_mask_bit(5, true)
		
		match ledge:
			LEFT_FLOOR:
				raycast.cast_to = Vector2(100,0)
				get_node("Detectors/LeftFloorLedge").add_child(raycast)
			RIGHT_FLOOR:
				raycast.cast_to = Vector2(-100,0)
				get_node("Detectors/RightFloorLedge").add_child(raycast)
			LEFT_WALL:
				raycast.cast_to = Vector2(0,100)
				get_node("Detectors/LeftWallLedge").add_child(raycast)
			RIGHT_WALL:
				raycast.cast_to = Vector2(0,100)
				get_node("Detectors/RightWallLedge").add_child(raycast)
			_:
				return 0
		
		raycast.force_raycast_update()
		if not raycast.is_colliding():
			return 0
		var body = raycast.get_collider()
		
		var distance = raycast.get_collision_point() - raycast.get_global_transform().origin
		raycast.queue_free()
		return distance.length()



# Methods to check if the creature is touching spikes.
func is_touching_down_spikes() -> bool:
	for body in get_down_bodies():
		if body.is_in_group("spikes"):
			return true
	return false

func is_touching_left_spikes() -> bool:
	for body in get_left_bodies():
		if body.is_in_group("spikes"):
			return true
	return false

func is_touching_up_spikes() -> bool:
	for body in get_up_bodies():
		if body.is_in_group("spikes"):
			return true
	return false

func is_touching_right_spikes() -> bool:
	for body in get_right_bodies():
		if body.is_in_group("spikes"):
			return true
	return false

func is_touching_spikes() -> bool:
	return is_touching_down_spikes() or is_touching_left_spikes() or is_touching_right_spikes() or is_touching_up_spikes()

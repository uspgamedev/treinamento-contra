extends KinematicBody2D
class_name BaseProjectile

export(float) var speed
export(float) var life_time
export(String) var hurt_group
export(int) var damage

onready var main: Node = get_tree().get_nodes_in_group("main")[0]

var control: float
var shoot_direction: Vector2

func _ready():
	$LifeTimer.wait_time = life_time
	$LifeTimer.start()

func _physics_process(delta):
	control = main.control
	move_and_slide(shoot_direction*speed*control)

func _on_LifeTimer_timeout():
	destroy()

func destroy():
	queue_free()

func _on_HurtBox_body_entered(body):
	if body.is_in_group(hurt_group):
		body.take_damage(damage, (body.position - self.position).normalized() )
		destroy()
	elif body.is_in_group("block"):
		destroy()



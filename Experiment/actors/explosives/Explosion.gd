extends Node2D

export (int) var damage_to_player
export (int) var damage_to_enemies
export (float) var knockback
export (float) var screenshake_magnitude

func _ready():
	$Explosion.current_animation = "Explosion"
	var camera = $Finder.get("camera")
	if $VisibilityNotifier2D.is_on_screen():
		camera.shake_camera(screenshake_magnitude)

func body_detected(body):
	var knockback_vector = body.position - self.position
	knockback_vector = knockback_vector.normalized()
	
	knockback_vector.x = clamp(knockback_vector.x, -0.25,  0.25)*4*knockback
	if knockback_vector.y < 0.8:
		knockback_vector.y = -clamp(knockback_vector.y, 0.6, 1.0)*knockback
	else:
		knockback_vector.y = 0.0
	
	if body.is_in_group("player"):
		body.take_damage(damage_to_player, knockback_vector.normalized(), knockback_vector.length())
	elif body.is_in_group("enemy") or body.is_in_group("explosive"):
		body.take_damage(damage_to_enemies, knockback_vector)

func _on_Explosion_animation_finished(anim_name):
	queue_free()

func _on_Sprite_frame_changed():
	if $Sprite.frame == 2:
		$Area.monitoring = true
	elif $Sprite.frame == 5:
		$Area.monitoring = false
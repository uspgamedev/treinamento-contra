extends BaseExplosive



export (PackedScene) var smoke_trail
export (float) var gravity
export (float) var max_falling_speed
export (float) var horizontal_damp
export (float) var life_time
export (Vector2) var initial_velocity
export (bool) var produce_smoke
export (bool) var turn_sprite

onready var velocity: Vector2 = initial_velocity



func _ready():
	$LifeTimer.wait_time = life_time
	$LifeTimer.start()

func _physics_process(delta):
	control = main.control
	
	if turn_sprite == true and velocity != Vector2.ZERO:
		self.rotation_degrees = rad2deg(velocity.angle()) - 90
	
	vertical_movement(delta)
	horizontal_movement(delta)
	
	velocity = move_and_slide(velocity*control)
	velocity /= control
	
	if produce_smoke == true:
		spawn_smoke()



func vertical_movement(delta) -> void:
	velocity.y += gravity*delta*control
	velocity.y = clamp(velocity.y, -MAX_SPEED, max_falling_speed)

func horizontal_movement(delta) -> void:
	if velocity.x > 0:
		velocity.x -= horizontal_damp*delta*control
		velocity.x = clamp(velocity.x, 0, MAX_SPEED)
	elif velocity.x < 0:
		velocity.x += horizontal_damp*delta*control
		velocity.x = clamp(velocity.x, -MAX_SPEED, 0)



# This produces a puff of smoke every frame on the missile's propeller.
func spawn_smoke() -> void:
	var smoke = smoke_trail.instance()
	smoke.position.x = self.position.x
	smoke.position.y = self.position.y - 16
	get_parent().add_child(smoke)



# When life_time seconds have passed since the bomb spawned, it will explode.
func on_timeout():
	self.explode()
extends KinematicBody2D

class_name BaseExplosive

const MAX_SPEED: float = 3000.0

export (PackedScene) var explosion

onready var control: float = 1.0
onready var is_exploding: bool = false
onready var main: Node = get_tree().get_nodes_in_group("main")[0]

signal exploded

func explode():
	if not is_exploding:
		is_exploding = true
		var blast = explosion.instance()
		blast.position = self.position
		get_parent().add_child(blast)
		emit_signal("exploded")
		queue_free()



func on_body_detected(body):
	if body.is_in_group("player") or body.is_in_group("explosive") or body.is_in_group("block"):
		if body != self:
			call_deferred("explode")

############Alias to reduce code on other areas ##########################
func take_damage(damage_amount:int = 0, source_direction := Vector2(), knockback_force:float = 0.0) -> void:
	explode()



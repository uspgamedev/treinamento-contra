extends BaseExplosive


export (float) var speed
export (float) var angular_acceleration
export (float) var max_angular_speed
export (Vector2) var starting_direction
export (float) var life_time
export (PackedScene) var smoke_trail

onready var velocity: Vector2 = Vector2.ZERO
onready var angular_speed: float = 0.0
onready var player: Node = $Finder.get("player")



func _ready():
	$LifeTimer.wait_time = life_time
	$LifeTimer.start()
	velocity = starting_direction.normalized()*speed



# The missile's intention is to always keep the same speed, but turn its velocity vector every turn
# in order to follow the player. To do this, we keep track of its angular_speed and rotate the
# missile's velocity every frame according to its angular speed. We apply the angular_acceleration
# every frame to update angular_speed according to where the player is located.
func _physics_process(delta):
	control = main.control
	
	seek_player(delta)
	turn(delta)
	
	self.rotation_degrees = rad2deg(velocity.angle())
	velocity = move_and_slide(velocity*control)
	velocity /= control
	
	produce_smoke()



# This method checks if the missile needs to turn clockwise (positive angle) or counter-clockwise
# (negative angle) and applies the correct acceleration it needs to turn correctly.
func seek_player(delta) -> void:
	if angle_between(player.position - self.position, velocity) >= 0:
		angular_speed += angular_acceleration*delta*control
	else:
		angular_speed -= angular_acceleration*delta*control
	angular_speed = clamp(angular_speed, -max_angular_speed, max_angular_speed)

# This method rotates the velocity vector according to the missile's current angular speed.
func turn(delta) -> void:
	velocity = velocity.rotated(angular_speed*delta*control)

# This method calculates the smallest angle from vector u to vector v.
func angle_between(u: Vector2, v: Vector2) -> float:
	var angle = u.angle() - v.angle()
	
	# If the angle is greater than 180 degrees, this code takes the counter-clockwise angle instead.
	if angle >= PI:
		angle -= 2*PI
	elif angle <= -PI:
		angle += 2*PI
	
	return angle



# This produces a puff of smoke every frame on the missile's propeller.
func produce_smoke() -> void:
	var smoke = smoke_trail.instance()
	smoke.position = self.position - self.velocity.normalized()*20
	get_parent().add_child(smoke)



# When life_time seconds have passed since the missile spawned, it will explode.
func on_timeout():
	self.explode()

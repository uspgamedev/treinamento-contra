extends BaseExplosive

export(float) var time_to_explode

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		$ExplosionTimer.start(time_to_explode)

func take_damage(damage_amount:int = 0, source_direction := Vector2(), knockback_force:float = 0.0) -> void:
	print("exploded")
	if $ExplosionTimer.is_stopped():
		$ExplosionTimer.start(time_to_explode)


func _on_ExplosionTimer_timeout():
	explode()

func explode():
	if not is_exploding:
		is_exploding = true
		var blast = explosion.instance()
		blast.get_node("Area/CollisionShape2D").shape.set_radius(68)
		blast.position = self.position
		get_parent().add_child(blast)
		emit_signal("exploded")
		queue_free()

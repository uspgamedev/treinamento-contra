extends BaseExplosive



export (float) var gravity
export (float) var damping

onready var velocity: Vector2 = Vector2.ZERO

func _physics_process(delta):
	control = main.control
	
	falling(delta)
	friction(delta)
	
	velocity = move_and_slide(velocity*control)
	velocity /= control



func falling(delta) -> void:
	velocity.y += gravity*delta*control

func friction(delta) -> void:
	if velocity.x > 0:
		velocity.x -= damping*delta*control
		velocity.x = clamp(velocity.x, 0, MAX_SPEED)
	elif velocity.x < 0:
		velocity.x += damping*delta*control
		velocity.x = clamp(velocity.x, -MAX_SPEED, 0)
extends PlayerState



export (PackedScene) var death_particles

onready var tween = player.get_node("Tween")

func _ready():
	self.this_state = player.DYING_STATE
	player.gravity = player.base_gravity/4
	player.max_natural_speed = player.move_speed/4
	player.acceleration = player.base_acceleration
	player.jump_force = player.base_jump_force
	player.max_falling_speed = player.base_falling_speed/4
	player.max_rising_speed = player.MAX_SPEED
	player.weight = 0.5
	sprite.get_node("GunSprite").visible = false
	tween.interpolate_property(sprite.get_material(), "shader_param/color",
										Color(0,0,0,1), Color(1,1,1,1), 1.5,
										Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	player.add_child(death_particles.instance())
	$Timer.start(0.5)

func _on_Timer_timeout():
	player.get_node("Finder").get("main").load_game_over_screen()

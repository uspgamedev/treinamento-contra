extends PlayerState




func _ready():
	self.this_state = player.DIVING_STATE
	player.move = 0.0
	player.max_falling_speed = player.diving_speed
	player.velocity = Vector2(0, player.diving_speed)


func _physics_process(_delta):
	end_dive()
	bounce()



func end_dive() -> void:
	if not player.is_airborne():
		player.change_state(player.BASE_STATE)

func bounce() -> void:
	if player.has_bounceables_underneath():
		player.is_jumping = true
		player.can_dodge = true
		player.is_bouncing = true
		player.get_node("AnimationPlayer").play("Bouncing")
		player.bounce_muzzle_flash()
		player.get_node("Camera").shake_camera(15)
		player.velocity.y = -player.jump_force
		sprite.bounce_particles()
		for bullet in player.get_bounceables():
			bullet.destroy()
		player.change_state(player.BASE_STATE)
extends PlayerState

onready var can_coyote_jump: bool = false


func _ready():
	self.this_state = player.BASE_STATE
	player.gravity = player.base_gravity
	player.max_natural_speed = player.move_speed
	player.acceleration = player.base_acceleration
	player.jump_force = player.base_jump_force
	player.max_falling_speed = player.base_falling_speed
	player.max_rising_speed = player.MAX_SPEED
	$EarlyJumpTimer.wait_time = player.early_jump_time
	$CoyoteJumpTimer.wait_time = player.coyote_jump_time
	player.get_node("Sprite").visible = true

func _physics_process(_delta):
	
	if not player.is_wall_jumping:
		move_player()
	
	if player.is_on_floor():
		can_coyote_jump = true
	
	if player.is_on_floor() or player.is_on_wall():
		player.can_dodge = true
	
	jump()
	early_jump()
	coyote_jump()
	dropping()
	bouncing()
	gliding()
	diving()
	cling_to_walls_and_ledges()



##### This method handles moving the player ###############################
func move_player() -> void:
	player.move = player.directional_input_actual.x
	if player.move != 0:
		player.facing = player.move/abs(player.move)
###########################################################################



##### These methods handle the basic jump #################################
func jump():
	if Input.is_action_just_pressed("ui_jump") and player.is_on_floor():
		player.velocity.y = -player.jump_force
		player.is_jumping = true
		sprite.jump_particles()
	elif player.is_jumping and Input.is_action_just_released("ui_jump"):
		player.velocity.y = 0
		can_coyote_jump = false
		player.is_jumping = false
		player.is_wall_jumping = false
	elif player.is_jumping and player.velocity.y >= 0:
		can_coyote_jump = false
		player.is_jumping = false
		player.is_wall_jumping = false
	if player.is_wall_jumping and ((player.facing == 1.0 and not player.is_close_to_left_wall()) or (player.facing == -1.0 and not player.is_close_to_right_wall())):
		if player.directional_input_actual.x != player.facing:
			player.is_wall_jumping = false

# This makes jumping right after landing feel better.
func early_jump() -> void:
	if not player.is_on_floor() and Input.is_action_just_pressed("ui_jump"):
		$EarlyJumpTimer.start()
	elif player.is_on_floor() and not $EarlyJumpTimer.is_stopped() and Input.is_action_pressed("ui_jump"):
		$EarlyJumpTimer.stop()
		player.velocity.y = -player.jump_force
		player.is_jumping = true
		sprite.jump_particles()
	elif not $EarlyJumpTimer.is_stopped() and not Input.is_action_pressed("ui_jump"):
		$EarlyJumpTimer.stop()

# This makes jumping off of ledges feel better.
func coyote_jump() -> void:
	if not player.is_on_floor() and player.force == Vector2.ZERO and can_coyote_jump and not player.is_jumping and $CoyoteJumpTimer.is_stopped():
		$CoyoteJumpTimer.start()
		can_coyote_jump = false
	if not $CoyoteJumpTimer.is_stopped() and Input.is_action_just_pressed("ui_jump"):
		$CoyoteJumpTimer.stop()
		player.velocity.y = -player.jump_force
		player.is_jumping = true
		sprite.coyote_jump_particles()
###########################################################################



##### This methods handles dropping off of platforms and ledges ###########
func dropping() -> void:
	if player.is_on_platform() and player.directional_input_actual.y == 1:
		player.drop_off_of_platform()
	elif not player.is_on_platform() and player.is_on_floor_ledge() and player.directional_input_actual == Vector2(0,1) and player.directional_input_old != Vector2(0,1):
		var ledge
		if player.is_on_left_floor_ledge():
			ledge = LEFT_FLOOR
		elif player.is_on_right_floor_ledge():
			ledge = RIGHT_FLOOR
		if player.get_ledge_distance(ledge) > 16:
			player.change_state(player.LEDGE_DROPPING_STATE)
###########################################################################



##### This method handles jumping off of opposing projectiles #############
func bouncing() -> void:
	if player.has_bounceables_underneath() and (not $EarlyJumpTimer.is_stopped() or Input.is_action_just_pressed("ui_jump")):
		player.is_jumping = true
		player.can_dodge = true
		player.is_bouncing = true
		player.get_node("AnimationPlayer").play("Bouncing")
		player.bounce_muzzle_flash()
		player.get_node("Camera").shake_camera(15)
		player.velocity.y = -player.jump_force
		sprite.bounce_particles()
		$EarlyJumpTimer.stop()
		for bullet in player.get_bounceables():
			bullet.destroy()
###########################################################################



##### This method handles the gliding feature #############################
func gliding() -> void:
	if Input.is_action_pressed("ui_jump") and not (player.is_on_floor() or player.is_on_wall()) and $EarlyJumpTimer.is_stopped() and $CoyoteJumpTimer.is_stopped() and not player.is_jumping:
		player.change_state(player.GLIDING_STATE)
###########################################################################



##### This method handles the transition to the diving state ##############
func diving() -> void:
	if Input.is_action_just_pressed("ui_jump") and not (player.is_on_floor() or player.is_on_wall()) and player.directional_input_actual.y == 1:
		player.change_state(player.DIVING_STATE)
###########################################################################



##### These methods handles climbing and grabbing ledges ##################
func cling_to_walls_and_ledges() -> void:
	if not player.is_airborne() and not player.is_on_floor():
		if player.is_on_wall_ledge():
			deal_with_ledges()
		elif player.is_on_wall() and not player.is_on_wall_ledge() and not player.is_jumping:
			player.change_state(player.WALL_CLINGING_STATE)

func deal_with_ledges() -> void:
	var ledge
	if player.is_on_left_wall_ledge():
		ledge = LEFT_WALL
	elif player.is_on_right_wall_ledge():
		ledge = RIGHT_WALL
	if player.get_ledge_distance(ledge) <= 24:
			player.position.y += player.get_ledge_distance(ledge)
			player.change_state(player.LEDGE_GRABBING_STATE)
	elif player.get_ledge_distance(ledge) > 24 and player.directional_input_actual.x == ledge.x:
		player.change_state(player.LEDGE_CLIMBING_STATE)
###########################################################################







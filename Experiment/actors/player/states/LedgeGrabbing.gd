extends PlayerState




func _ready():
	self.this_state = player.LEDGE_GRABBING_STATE
	player.gravity = 0.0
	player.velocity = Vector2(0,0)
	player.force = Vector2(0,0)
	player.can_dodge = true
	
	if player.is_on_left_wall_ledge():
		player.facing = 1.0
	elif player.is_on_right_wall_ledge():
		player.facing = -1.0


func _physics_process(_delta):
	climbing()
	wall_jumping()
	letting_go()


func climbing() -> void:
	if player.directional_input_actual.x >= -player.facing and player.directional_input_actual.y <= 0 and player.directional_input_actual != Vector2(0,0) and player.directional_input_old != player.directional_input_actual:
		player.change_state(player.LEDGE_CLIMBING_STATE)


func wall_jumping() -> void:
	if Input.is_action_just_pressed("ui_jump") or (player.directional_input_actual.x == player.facing and player.directional_input_old.x != player.facing):
		player.is_jumping = true
		player.is_wall_jumping = true
		player.velocity = Vector2( player.facing*player.move_speed , -player.wall_jump_multiplier*player.jump_force)
		player.move = player.facing
		sprite.wall_jump_particles()
		player.change_state(player.BASE_STATE)


func letting_go() -> void:
	if player.directional_input_actual == Vector2(0,1) and player.directional_input_old != Vector2(0,1):
		player.position.x += player.facing*5
		player.change_state(player.BASE_STATE)
	
	if player.is_on_floor():
		player.change_state(player.BASE_STATE)


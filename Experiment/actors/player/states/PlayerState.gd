extends Node
class_name PlayerState



const LEFT_FLOOR = Vector2(-1,1)
const RIGHT_FLOOR = Vector2(1,1)
const LEFT_WALL = Vector2(-1,-1)
const RIGHT_WALL = Vector2(1,-1)


onready var player: Node = get_parent().get_parent()
onready var level: Node = get_tree().get_nodes_in_group("stage")[0]
onready var sprite: Sprite = player.get_node("Sprite")

var this_state: int



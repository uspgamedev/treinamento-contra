extends PlayerState

export (PackedScene) var particles

onready var dodge_speed = player.dodge_distance / player.dodge_time

func _ready():
	self.this_state = player.DODGING_STATE
	player.max_falling_speed = 0.0
	player.max_natural_speed = 0.0
	player.gravity = 0.0
	player.acceleration = 0.0
	player.move = 0.0
	sprite.visible = false
	
	player.set_collision_layer_bit(0, false)
	player.set_collision_mask_bit(0, false)
	player.set_collision_layer_bit(2, false)
	player.set_collision_mask_bit(2, false)
	sprite.z_index = -3
	$Timer.start(player.dodge_time)
	sprite.dodge_particles()

func _physics_process(_delta):
	player.can_dodge = false
	player.velocity = Vector2(0,0)
	player.force = dodge_speed * player.dodge_direction.normalized()
	spawn_particles()

func spawn_particles() -> void:
	var trail: Particles2D = particles.instance()
	if player.facing == -1:
		trail.transform.x *= -1
	trail.position = player.position
	level.add_child(trail)


func _on_Timer_timeout():
	player.force = Vector2(0,0)
	player.set_collision_layer_bit(0, true)
	player.set_collision_mask_bit(0, true)
	if not is_on_platform():
		player.set_collision_layer_bit(2, true)
		player.set_collision_mask_bit(2, true)
	sprite.z_index = 1
	sprite.visible = true
	sprite.dodge_particles()
	player.change_state(player.BASE_STATE)

func is_on_platform() -> bool:
	for body in player.get_middle_bodies():
		if body.is_in_group("platform"):
			return true
	return false





extends PlayerState


const FACTOR: float = 3.0

func _ready():
	self.this_state = player.GLIDING_STATE
	player.max_falling_speed = player.gliding_speed
	player.max_natural_speed /= FACTOR
	player.acceleration /= FACTOR
	player.force.x += player.velocity.x
	player.velocity.x = 0.0

func _physics_process(_delta):
	move_player()
	end_glide()



func end_glide() -> void:
	if Input.is_action_just_released("ui_jump") or player.is_on_floor() or player.is_on_wall():
		player.change_state(player.BASE_STATE)

func move_player() -> void:
	player.move = player.directional_input_actual.x
	if player.move != 0:
		player.facing = player.move/abs(player.move)
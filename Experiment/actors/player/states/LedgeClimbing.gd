extends PlayerState



var can_end_animation

func _ready():
	self.this_state = player.LEDGE_CLIMBING_STATE
	player.gravity = player.base_gravity
	player.max_natural_speed = player.move_speed/2
	player.acceleration = player.base_acceleration
	player.jump_force = player.base_jump_force
	player.max_falling_speed = player.base_falling_speed
	player.max_rising_speed = player.MAX_SPEED
	
	if player.is_on_left_wall_ledge():
		player.facing = 1.0
	elif player.is_on_right_wall_ledge():
		player.facing = -1.0
	
	player.velocity = Vector2(0, -450)
	player.move = -player.facing
	can_end_animation = false


func _physics_process(_delta):
	if player.velocity.y >= 0 and not can_end_animation:
		can_end_animation = true
	
	if can_end_animation and (player.directional_input_actual != Vector2(0,0) or player.is_on_floor()):
		player.change_state(player.BASE_STATE)
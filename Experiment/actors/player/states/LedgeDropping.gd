extends PlayerState



func _ready():
	self.this_state = player.LEDGE_DROPPING_STATE
	player.gravity = player.base_gravity
	player.max_natural_speed = player.move_speed
	player.acceleration = player.base_acceleration
	player.jump_force = player.base_jump_force
	player.max_falling_speed = player.base_falling_speed
	player.max_rising_speed = player.MAX_SPEED
	
	if player.is_on_left_floor_ledge():
		player.facing = -1.0
	elif player.is_on_right_floor_ledge():
		player.facing = 1.0
	
	player.move = player.facing


func _physics_process(_delta):
	if not player.is_on_floor() and $Timer.is_stopped():
		player.velocity = Vector2(0,0)
		player.move *= -1
		$Timer.start()

func _on_Timer_timeout():
	player.change_state(player.BASE_STATE)

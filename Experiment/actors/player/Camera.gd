extends Camera2D



export (float, 1000) var limit_change_smoothing

onready var is_shaking: bool = false
onready var player: KinematicBody2D = get_parent()
onready var shake_magnitude: float = 0.0
onready var shake_slowdown: float = 100     # Magnitude is lowered by this amount each second

onready var new_top_limit: float = -1000000
onready var new_bottom_limit: float = 1000000
onready var new_right_limit: float = 1000000
onready var new_left_limit: float = -1000000
onready var camera_started: bool = false

onready var main = get_tree().get_nodes_in_group("main")[0]


func _physics_process(delta):
	camera_shake(delta)
	
	if player.is_airborne():
		air_camera(delta)
	else:
		std_camera()
	
	if camera_started:
		if limit_top < new_top_limit:
			limit_top += delta*limit_change_smoothing
			limit_top = clamp(limit_top, -10000000, new_top_limit)
		elif limit_top > new_top_limit:
			limit_top -= delta*limit_change_smoothing
			limit_top = clamp(limit_top, new_top_limit, 10000000)
		
		if limit_bottom < new_bottom_limit:
			limit_bottom += delta*limit_change_smoothing
			limit_bottom = clamp(limit_bottom, -10000000, new_bottom_limit)
		elif limit_bottom > new_bottom_limit:
			limit_bottom -= delta*limit_change_smoothing
			limit_bottom = clamp(limit_bottom, new_bottom_limit, 10000000)
		
		if limit_right < new_right_limit:
			limit_right += delta*limit_change_smoothing
			limit_right = clamp(limit_right, -10000000, new_right_limit)
		elif limit_right > new_right_limit:
			limit_right -= delta*limit_change_smoothing
			limit_right = clamp(limit_right, new_right_limit, 10000000)
		
		if limit_left < new_left_limit:
			limit_left += delta*limit_change_smoothing
			limit_left = clamp(limit_left, -10000000, new_left_limit)
		elif limit_left > new_left_limit:
			limit_left -= delta*limit_change_smoothing
			limit_left = clamp(limit_left, new_left_limit, 10000000)
		
	elif new_top_limit != -1000000:
		limit_top = new_top_limit
		limit_bottom = new_bottom_limit
		limit_left = new_left_limit
		limit_right = new_right_limit
		camera_started = true



func camera_shake(delta: float) -> void:
	if is_shaking and !main.no_shake:
		var camera_offset = Vector2()
		camera_offset.x = rand_range(-1.0, 1.0)
		camera_offset.y = rand_range(-1.0, 1.0)
		camera_offset = camera_offset.normalized()
		self.offset = camera_offset * shake_magnitude
		
		shake_magnitude -= delta*shake_slowdown
		if shake_magnitude <= 0:
			shake_magnitude = 0
			is_shaking = false
			self.offset = Vector2.ZERO

func shake_camera(magnitude: float) -> void:
	if not is_shaking:
		shake_magnitude += magnitude
		is_shaking = true
	else:
		if shake_magnitude < 100:
			shake_magnitude += magnitude/2
		elif shake_magnitude >= 100 and shake_magnitude < 150:
			shake_magnitude += magnitude/4
			shake_magnitude = clamp(shake_magnitude, 0, 150)



func air_camera(delta) -> void:
	if player.velocity.y < player.base_falling_speed*0.9:
		self.drag_margin_left = 1.0
		self.drag_margin_right = 1.0
		self.drag_margin_bottom = 1.0
		self.drag_margin_top = 1.0
		self.smoothing_speed = 3
	else:
		self.drag_margin_left = 0.0
		self.drag_margin_right = 0.0
		self.drag_margin_bottom = 0.0
		self.drag_margin_top = 0.0
		self.smoothing_speed = clamp(smoothing_speed + delta*4, 3, 7)

func std_camera() -> void:
	self.drag_margin_left = 0.0
	self.drag_margin_right = 0.0
	self.drag_margin_bottom = 0.0
	self.drag_margin_top = 0.0
	self.smoothing_speed = 3



extends Sprite



export (PackedScene) var dust_particles
export (PackedScene) var damage_particles

onready var player = get_parent()
onready var level = get_tree().get_nodes_in_group("stage")[0]
onready var main = get_tree().get_nodes_in_group("main")[0]



# This block handles the particles released when taking damage.
func release_damage_particles() -> void:
	if main.no_particles:
		return
	var puffs = damage_particles.instance()
	puffs.position = player.position
	level.add_child(puffs)

# This block handles the dust produced by walking.
func on_frame_changed():
	var frame = get_frame()
	if main.no_particles:
		return
	
	if frame == 2 or frame == 3 or frame == 4 or frame == 5:
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-12*player.facing, 24)
		level.add_child(dust)

# This block handles the dust produced by sliding down walls.
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "WallClinging":
		if !main.no_particles:
			var dust = dust_particles.instance()
			dust.position = player.position + Vector2( -20*player.facing, -22)
			level.add_child(dust)
		if player.get_state() == player.WALL_CLINGING_STATE:
			player.get_node("AnimationPlayer").play("WallClinging")

# This block handles the dust produced by jumping.
func jump_particles() -> void:
	if main.no_particles:
		return
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i, 20 + -6)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i + 4, 20 + 0)
		level.add_child(dust)
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i, 20 + 6)
		level.add_child(dust)

func coyote_jump_particles() -> void:
	if main.no_particles:
		return
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i - 18*player.facing, 20 + -6)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i + 4 - 18*player.facing, 20 + 0)
		level.add_child(dust)
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i - 18*player.facing, 20 + 6)
		level.add_child(dust)

# This block handles the dust produced by wall jumping.
func wall_jump_particles() -> void:
	if main.no_particles:
		return
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-20*player.facing + -6, i + 16)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-20*player.facing + 0, i + 22)
		level.add_child(dust)
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-20*player.facing + 6, i + 16)
		level.add_child(dust)

# This block handles the dust produced by dodging.
func dodge_particles() -> void:
	if main.no_particles:
		return
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-9,i + 16)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(-3,i + 22)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(3,i + 16)
		level.add_child(dust)
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(9,i + 22)
		level.add_child(dust)

# This block handles the dust produced by bouncing.
func bounce_particles() -> void:
	if main.no_particles:
		return
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i + 16, 20 + -6)
		level.add_child(dust)
	for i in range(-24, 24, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i + 22, 20 + 0)
		level.add_child(dust)
	for i in range(-16, 16, 8):
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(i + 16, 20 + 6)
		level.add_child(dust)

# This block handles player transparency when invincible after taking damage.
func player_invincibility_ended():
	self.modulate = Color(1,1,1,1)



func _physics_process(delta):
	
	# This block handles the dust produced when airborne.
	if main.no_particles:
		return
	if player.get_state() == player.BASE_STATE and player.is_airborne() and $AirborneDust.is_stopped():
		$AirborneDust.start(0.05)
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(0, 20)
		level.add_child(dust)
	elif player.get_state() == player.GLIDING_STATE and $AirborneDust.is_stopped():
		$AirborneDust.start(0.1)
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(0, 20)
		level.add_child(dust)
	elif player.get_state() == player.DIVING_STATE:
		var dust = dust_particles.instance()
		dust.position = player.position + Vector2(0, -12)
		level.add_child(dust)
	
	# This block handles the jet pack particles when gliding.
	if player.get_state() == player.GLIDING_STATE:
		$JetParticles.emitting = true
	else:
		$JetParticles.emitting = false
	
	
	


extends BaseGun



export (float, 100) var projectile_lifetime
export (float, 180) var spread_angle
export (float, 180) var spread_speed



onready var base_spread_angle: float = deg2rad(spread_angle)
onready var base_spread_speed: float = deg2rad(spread_speed)
onready var actual_spread: float = 0.0

func fire():
	aim_gun()
	
	if self.can_fire():
		var bullet = projectile.instance()
		bullet.position = player.position + player.muzzle*barrel_size
		bullet.speed = projectile_speed
		bullet.damage = projectile_damage
		bullet.shoot_direction = player.shooting_direction.rotated(actual_spread).normalized()
		bullet.life_time = projectile_lifetime
		bullet.knockback = self.knockback
		bullet.particles = self.bullet_fragments
		level.add_child(bullet)
		spread()
		
		shake_screen()
		muzzle_flash_form()
		
		self.start_cooldown()
		$ResetTimer.start()
		
		if player.is_airborne():
			player.force += -recoil*player.shooting_direction.normalized()

func muzzle_flash_form() -> void:
	spawn_muzzle_flash_light(1.0, 1.0, Vector2.ZERO, 0.1)
	spawn_muzzle_flash(4, 18, Vector2.ZERO, 0.1)
	spawn_muzzle_flash(6, 8, Vector2(8,0), 0.1)
	spawn_muzzle_flash(4, 20, Vector2(20,0), 0.1)
	spawn_muzzle_flash(6, 8, Vector2(28,0), 0.1)
	spawn_muzzle_flash(4, 16, Vector2(40,0), 0.1)
	spawn_muzzle_flash(6, 6, Vector2(48,0), 0.1)
	spawn_muzzle_flash(4, 12, Vector2(60,0), 0.1)
	spawn_muzzle_flash(4, 4, Vector2(68,0), 0.1)



func spread() -> void:
	actual_spread += base_spread_speed
	
	if actual_spread > base_spread_angle/2:
		actual_spread = base_spread_angle/2 - (actual_spread - base_spread_angle/2)
		base_spread_speed *= -1.0
	
	if actual_spread < -base_spread_angle/2:
		actual_spread = -base_spread_angle/2 - (actual_spread + base_spread_angle/2)
		base_spread_speed *= -1.0

func _on_ResetTimer_timeout():
	actual_spread = 0.0
	if base_spread_speed < 0:
		base_spread_speed *= -1

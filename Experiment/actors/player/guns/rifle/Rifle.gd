extends BaseGun

onready var collision_point: Vector2 = Vector2.ZERO
onready var smoke_trail: Vector2 = Vector2.ZERO
onready var hit: bool = false



func fire() -> void:
	aim_gun()

	if self.can_fire():
		$Aim.enabled = true
		$Aim.clear_exceptions()
		$Aim.cast_to = player.muzzle*1100
		$Aim.force_raycast_update()
		var target = $Aim.get_collider()
		collision_point = Vector2.ZERO

		# We use the raycast $Aim to check if the player hit something. On the while
		# block next, we check if the player hit an enemy or anything that blocks bullets.
		# If it hit any of those things, the while breaks. If it hits something else, this
		# something else is ignored and it checks if it is hitting something else. If
		# nothing was hit, the while breaks.
		while collision_point == Vector2.ZERO:
			$Aim.force_raycast_update()

			# Dealing with detected bodies.
			if $Aim.is_colliding():
				target = $Aim.get_collider()
				if target.is_in_group("block") or target.is_in_group("assymetric_block"):
					collision_point = $Aim.get_collision_point()
					smoke_trail = collision_point - (player.get_global_transform().origin + player.muzzle*barrel_size)
					hit = true
				elif target.is_in_group("enemy") or target.is_in_group("explosive"):
					collision_point = $Aim.get_collision_point()
					target.take_damage(self.projectile_damage, player.shooting_direction.normalized(), knockback)
					smoke_trail = collision_point - (player.get_global_transform().origin + player.muzzle*barrel_size)
					hit = true
				else:
					$Aim.add_exception(target)
					collision_point = Vector2.ZERO

			# Dealing with a miss.
			else:
				collision_point = $Aim.cast_to
				smoke_trail = collision_point - player.muzzle*barrel_size
				hit = false

		shake_screen()
		smoke_effects()
		muzzle_flash_form()

		if hit:
			hit_particles()

		start_cooldown()
		$Aim.enabled = false

		if player.is_airborne():
			player.force += -recoil*player.shooting_direction.normalized()
		elif player.is_on_floor():
			player.force.x += -(recoil/2)*player.shooting_direction.normalized().x

func muzzle_flash_form() -> void:
	spawn_muzzle_flash_light(2.0, 1.5)
	spawn_muzzle_flash(4, 16)
	spawn_muzzle_flash(4, 28, Vector2(8,0))
	spawn_muzzle_flash(4, 40, Vector2(16,0))
	spawn_muzzle_flash(4, 52, Vector2(24,0))
	spawn_muzzle_flash(12, 28, Vector2(32,0))
	spawn_muzzle_flash(24, 16, Vector2(56,0))
	spawn_muzzle_flash(30, 8, Vector2(104,0))



func smoke_effects() -> void:
	var trail: float = 0.0
	while trail <= smoke_trail.length():
		var smoke = projectile.instance()
		smoke.lifetime = 1.5
		smoke.position = player.position + player.muzzle*barrel_size + smoke_trail.normalized()*trail
		level.add_child(smoke)
		trail += 2 + randi()%6

func hit_particles() -> void:
	var particles = bullet_fragments.instance()
	particles.position = player.position + player.muzzle*barrel_size + smoke_trail
	particles.process_material.angle = rad2deg(smoke_trail.angle())
	particles.rotation_degrees = rad2deg(smoke_trail.angle())
	level.add_child(particles)




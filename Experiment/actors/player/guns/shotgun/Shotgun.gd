extends BaseGun



export (float, 1000) var weapon_range
export (float, 180) var spread_angle
export (int, 20) var   shots_fired



# The plan to make a cone of n shots is to generate n vectors with values between 0 and 1000
# for each coordinate. Then, we translate them in such a way that the distance from the origin
# to their center is equal to the velocity of the projectiles. Then, we shrink the distance of
# each vector from their center in a way such that the final vectors won't spread farther than
# the set spread angle from one another. Finally, we use each of these vectors as the velocity
# for each projectile fired.
func fire() -> void:
	if animation.get_current_animation() != "ReloadingShotgun":
		aim_gun()
	
	if self.can_fire():
		var vectors = []
		vectors = generate_vectors(shots_fired)
		
		# This scaling_factor is the factor that will resize the vectors.
		var scaling_factor = projectile_speed * tan(deg2rad(spread_angle/2)) / 1000
		
		vectors = escalate_vectors(vectors, scaling_factor)
		vectors = translate(vectors, -player.shooting_direction.normalized()*projectile_speed)
		
		for vector in vectors:
			var bullet = projectile.instance()
			bullet.position = player.position + player.muzzle*barrel_size
			bullet.speed = vector.length()
			bullet.damage = projectile_damage
			bullet.shoot_direction = vector.normalized()
			bullet.life_time = weapon_range/projectile_speed
			bullet.knockback = self.knockback
			bullet.particles = self.bullet_fragments
			level.add_child(bullet)
		
		shake_screen()
		muzzle_flash_form()
		
		self.start_cooldown()
		
		if player.is_airborne():
			player.force += -recoil*player.shooting_direction.normalized()
		elif player.is_on_floor():
			player.force.x += -(recoil/2)*player.shooting_direction.normalized().x

func muzzle_flash_form() -> void:
	spawn_muzzle_flash_light(2.0, 1.5)
	spawn_muzzle_flash(6, 20)
	spawn_muzzle_flash(6, 28, Vector2(12,0))
	spawn_muzzle_flash(12, 36, Vector2(24,0))
	spawn_muzzle_flash(6, 15, Vector2(48,0))
	spawn_muzzle_flash(6, 23, Vector2(60,0))
	spawn_muzzle_flash(6, 31, Vector2(72,0))
	spawn_muzzle_flash(16, 16, Vector2(84,0))
	spawn_muzzle_flash(16, 10, Vector2(116,0))
	spawn_muzzle_flash(16, 4, Vector2(148,0))



# These three methods generate the vectors in such a way that any three of them are not too close
# or aligned. This gives the impression that the random shots are more spread away from each other.
func generate_vectors(n):
	var velocities = []
	for bullet in range(n):
		var dir := Vector2()
		dir.x = randi()%1001
		dir.y = randi()%1001
		while check_alignments(velocities, dir):
			dir.x = randi()%1001
			dir.y = randi()%1001
		velocities.append(dir)
	return velocities

func check_alignments(list, bullet) -> bool:
	for i in range(list.size()):
		for j in range(i):
			if are_aligned(list[i], list[j], bullet):
				return true
	return false

# This method returns true if the vectors a, b and c form a triangle of too small an area.
# If the area is zero, this means that the three points are aligned.
func are_aligned(a, b, c) -> bool:
	var side1 = Vector3(a.x - c.x, a.y - c.y, 0)
	var side2 = Vector3(b.x - c.x, b.y - c.y, 0)
	var area = side1.cross(side2).length_squared()
	return area < 250

func escalate_vectors(list, factor):
	var center = take_baricenter(list)
	list = translate(list, center)
	return dilate(list, factor)

func take_baricenter(list):
	var center = Vector2(0,0)
	for vector in list:
		center += vector
	return center/list.size()

func translate(list, origin):
	var res = []
	for vector in list:
		res.append(vector - origin)
	return res

func dilate(list, factor):
	var res = []
	for vector in list:
		res.append(vector*factor)
	return res



# This plays the reloading animation.
func _on_Timer_timeout():
	if animation.get_current_animation() == "Shotgun":
		rest_position()
		animation.play("ReloadingShotgun")





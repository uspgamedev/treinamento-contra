extends BaseProjectile


var knockback: float
var particles: PackedScene

onready var gravity = Vector2(0,200)     # To set the gravity for the fragment particles


func _ready():
	self.rotation_degrees = rad2deg(shoot_direction.angle())

func _on_HurtBox_body_entered(body):
	if body.is_in_group(hurt_group) or body.is_in_group("explosive"):
		body.take_damage(damage, shoot_direction.normalized(), knockback)
	spawn_particles()
	destroy()

func spawn_particles() -> void:
	var fragments = particles.instance()
	fragments.emitting = true
	fragments.position = self.position
	fragments.rotation_degrees = rad2deg(shoot_direction.angle())
	fragments.process_material.angle = rad2deg(shoot_direction.angle())
	fragments.process_material.gravity = Vector3(
												gravity.rotated(-shoot_direction.angle()).x,
												gravity.rotated(-shoot_direction.angle()).y, 0)
	get_parent().add_child(fragments)
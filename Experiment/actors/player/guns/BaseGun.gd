extends Node
class_name BaseGun



export (PackedScene) var projectile
export (PackedScene) var muzzle_flash
export (PackedScene) var muzzle_flash_light
export (PackedScene) var bullet_fragments
export (float, 2000) var projectile_speed
export (int, 100) var projectile_damage
export (float, 100) var fire_rate
export (float, 1000) var knockback
export (float, 1000) var recoil
export (float, 150) var camera_shake_magnitude

onready var player = get_parent().get_parent()
onready var camera = player.get_node("Camera")
onready var gun = player.get_node("Sprite/GunSprite")
onready var animation = gun.get_node("GunAnimations")
onready var level = player.get_parent()
onready var cooldown = 1/fire_rate
onready var barrel_size = 40



func _process(delta):
	if gun.get_node("Timer").is_stopped() and not player.is_shooting:
		rest_position()

func start_cooldown() -> void:
	$Timer.start(cooldown)
	gun.get_node("Timer").start()

func can_fire() -> bool:
	if $Timer.is_stopped():
		return true
	else:
		return false

func shake_screen() -> void:
	camera.shake_camera(camera_shake_magnitude)

func spawn_muzzle_flash(x: float, y: float, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var flash = muzzle_flash.instance()
	flash.emission_rect_extents = Vector2(x,y)
	flash.lifetime = time
	flash.amount = 72*4*x*y/1024
	flash.position = Vector2(barrel_size + x, 0) + offset
	gun.add_child(flash)

func spawn_muzzle_flash_light(scale: float = 1.0, energy: float = 1.0, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var light = muzzle_flash_light.instance()
	light.texture_scale = scale
	light.init_energy = energy
	light.lifetime = time
	light.position = Vector2(barrel_size, 0) + offset
	gun.add_child(light)

func aim_gun() -> void:
	if player.muzzle.x < 0:
		gun.get_node("Sprite").flip_v = true
		gun.rotation_degrees = rad2deg(player.muzzle.angle())
	else:
		gun.get_node("Sprite").flip_v = false
		gun.rotation_degrees = rad2deg(player.muzzle.angle())

func rest_position() -> void:
	if player.facing > 0:
		gun.get_node("Sprite").flip_v = false
		gun.rotation_degrees = 0
	else:
		gun.get_node("Sprite").flip_v = true
		gun.rotation_degrees = 180

func fire() -> void:
	pass




extends BaseCreature
class_name Player



const THUMBSTICK_DEADZONE: float = 0.2

const BASE_STATE: int = 0
const WALL_CLINGING_STATE: int = 1
const LEDGE_GRABBING_STATE: int = 2
const LEDGE_CLIMBING_STATE: int = 3
const LEDGE_DROPPING_STATE: int = 4
const GLIDING_STATE: int = 5
const DIVING_STATE: int = 6
const DODGING_STATE: int = 7
const DYING_STATE: int = 8

const RIFLE: int = 0
const MACHINE_GUN: int = 1
const SHOTGUN: int = 2



export(float) var move_speed
export(float) var base_acceleration
export(float) var base_gravity
export(float) var jump_height
export(float) var gliding_speed
export(float) var diving_speed
export(float) var wall_jump_multiplier
export(float) var wall_sliding_speed
export(float) var dodge_distance
export(float) var dodge_time
export(float) var early_jump_time
export(float) var coyote_jump_time
export(float) var invincibility_time

onready var directional_input_actual := Vector2()
onready var directional_input_old := Vector2()
onready var base_jump_force: float = sqrt(2*jump_height*base_gravity)
onready var base_falling_speed: float = sqrt(2*jump_height*base_gravity)
onready var can_dodge: bool = false
onready var dodge_direction := Vector2()
onready var is_jumping: bool = false
onready var is_wall_jumping: bool = false
onready var is_bouncing: bool = false
onready var is_shooting: bool = false
onready var facing: float = 1.0
onready var shooting_direction := Vector2()
onready var muzzle := Vector2()
onready var equipped_gun: Node = null
onready var equipped_gun_index: int = 0



func _ready():
	self.gravity = base_gravity
	self.max_natural_speed = move_speed
	self.acceleration = base_acceleration
	self.jump_force = base_jump_force
	self.max_falling_speed = base_falling_speed
	self.max_rising_speed = MAX_SPEED
	self.force = Vector2(0,0)
	self.weight = 1.0
	$Sprite.get_material().set_shader_param("color", Color(0,0,0,1))
	$InvincibilityTimer.wait_time = invincibility_time
	change_state(BASE_STATE)
	change_gun(main.initial_gun)
	$Finder.get("hud").update_display()



func _physics_process(_delta):
	if main.no_coyote:
		coyote_jump_time = 0.01
	if main.no_bunny_hop:
		early_jump_time = 0.01
	get_directional_inputs()
	
	if can_dodge and Input.is_action_just_pressed("ui_dodge") and get_node("Sprite/GunSprite/Timer").is_stopped():
		dodging()
	
	if not get_state() == DYING_STATE and not get_state() == DODGING_STATE:
		shooting()
	
	changing_guns()
	deal_with_spikes()



# This function checks if the player is pressing any directional button or tilting the left analog stick.
# It takes care of diagonals, opposing directional inputs and everything, from both the buttons and the stick.
func get_directional_inputs() -> void:
	directional_input_old = directional_input_actual
	
	################### Getting from diretional buttons #####################
	directional_input_actual = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up"))
	#########################################################################
	
	################### Getting from thumbsticks ############################
	var stick_input = Vector2(
		Input.get_joy_axis(0, JOY_ANALOG_LX), 
		Input.get_joy_axis(0, JOY_ANALOG_LY))
	if abs(stick_input.x) >= THUMBSTICK_DEADZONE:
		directional_input_actual.x = abs(stick_input.x)/stick_input.x
	if abs(stick_input.y) >= THUMBSTICK_DEADZONE:
		directional_input_actual.y = abs(stick_input.y)/stick_input.y
	#########################################################################


# This method returns the current state.
func get_state() -> int:
	for state in $State.get_children():
		return state.this_state

# This method is used to switch states.
func change_state(new_state):
	for state in $State.get_children():
		state.queue_free()
	if new_state == BASE_STATE:
		new_state = preload("res://actors/player/states/Base.tscn").instance()
		$State.add_child(new_state)
	elif new_state == WALL_CLINGING_STATE:
		new_state = preload("res://actors/player/states/WallClinging.tscn").instance()
		$State.add_child(new_state)
	elif new_state == LEDGE_GRABBING_STATE:
		new_state = preload("res://actors/player/states/LedgeGrabbing.tscn").instance()
		$State.add_child(new_state)
	elif new_state == LEDGE_CLIMBING_STATE:
		new_state = preload("res://actors/player/states/LedgeClimbing.tscn").instance()
		$State.add_child(new_state)
	elif new_state == LEDGE_DROPPING_STATE:
		new_state = preload("res://actors/player/states/LedgeDropping.tscn").instance()
		$State.add_child(new_state)
	elif new_state == GLIDING_STATE:
		new_state = preload("res://actors/player/states/Gliding.tscn").instance()
		$State.add_child(new_state)
	elif new_state == DIVING_STATE:
		new_state = preload("res://actors/player/states/Diving.tscn").instance()
		$State.add_child(new_state)
	elif new_state == DODGING_STATE:
		new_state = preload("res://actors/player/states/Dodging.tscn").instance()
		$State.add_child(new_state)
	elif new_state == DYING_STATE:
		new_state = preload("res://actors/player/states/Dying.tscn").instance()
		$State.add_child(new_state)



# Dodging feature. If dodge_direction is zero, the player won't dodge. Otherwise, it will dodge
# to the direction the player is facing or the directional inputs the player is giving.
func dodging() -> void:
	var state = get_state()
	
	################### Getting from diretional inputs #####################
	if state == BASE_STATE or state == LEDGE_CLIMBING_STATE or state == LEDGE_DROPPING_STATE or state == GLIDING_STATE:
		if directional_input_actual != Vector2(0,0):
			dodge_direction = directional_input_actual
		else:
			dodge_direction = Vector2(facing , 0)
	########################################################################
	
	################### Getting from facing only ###########################
	elif state == WALL_CLINGING_STATE or state == LEDGE_GRABBING_STATE:
		dodge_direction = Vector2(facing , 0)
	########################################################################
	
	################### Can't dodge on these states ########################
	else:
		dodge_direction = Vector2(0,0)
	########################################################################
	
	if dodge_direction != Vector2(0,0):
		change_state(DODGING_STATE)



# Shooting feature:
func shooting() -> void:
	
	var direction = Vector2(
						Input.get_joy_axis(0, JOY_ANALOG_RX),
						Input.get_joy_axis(0, JOY_ANALOG_RY))
	
	if Input.is_action_pressed("ui_shoot"):
		shooting_direction = get_viewport().get_mouse_position() - self.get_global_transform_with_canvas().origin
		muzzle = shooting_direction.normalized()
		equipped_gun.fire()
		is_shooting = true
	elif direction.length() > THUMBSTICK_DEADZONE:
		shooting_direction = direction
		muzzle = direction.normalized()
		equipped_gun.fire()
		is_shooting = true
	else:
		is_shooting = false



# Changing weapons feature:
func changing_guns() -> void:
	if Input.is_action_just_pressed("ui_rifle"):
		change_gun(RIFLE)
	if Input.is_action_just_pressed("ui_machine_gun"):
		change_gun(MACHINE_GUN)
	if Input.is_action_just_pressed("ui_shotgun"):
		change_gun(SHOTGUN)

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_DOWN and $ChangeGunTimer.is_stopped():
			change_gun((equipped_gun_index + 1)%3)
			$ChangeGunTimer.start()
		elif event.button_index == BUTTON_WHEEL_UP and $ChangeGunTimer.is_stopped():
			change_gun((equipped_gun_index + 2)%3)
			$ChangeGunTimer.start()

func change_gun(new_gun: int) -> void:
	var animation = get_node("Sprite/GunSprite/GunAnimations")
	match new_gun:
		RIFLE:
			equipped_gun = get_node("Guns/Rifle")
			equipped_gun_index = RIFLE
			animation.play("Rifle")
		MACHINE_GUN:
			equipped_gun = get_node("Guns/MachineGun")
			equipped_gun_index = MACHINE_GUN
			animation.play("MachineGun")
		SHOTGUN:
			equipped_gun = get_node("Guns/Shotgun")
			equipped_gun_index = SHOTGUN
			animation.play("Shotgun")



# These next three methods work with spawning the bounce pistol's muzzle flash.
func bounce_muzzle_flash() -> void:
	spawn_muzzle_flash_light()
	spawn_muzzle_flash(8, 3)
	spawn_muzzle_flash(10, 3, Vector2(0,6))
	spawn_muzzle_flash(12, 6, Vector2(0,12))
	spawn_muzzle_flash(9, 3, Vector2(0,24))
	spawn_muzzle_flash(6, 3, Vector2(0,30))

func spawn_muzzle_flash(x: float, y: float, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var flash = get_node("Guns/MachineGun").muzzle_flash.instance()
	flash.emission_rect_extents = Vector2(x,y)
	flash.lifetime = time
	flash.amount = 72*4*x*y/1024
	flash.position = Vector2(facing*16, 32 + y) + offset
	self.add_child(flash)

func spawn_muzzle_flash_light(scale: float = 1.0, energy: float = 1.0, offset: Vector2 = Vector2.ZERO, time: float = 0.2) -> void:
	var light = get_node("Guns/MachineGun").muzzle_flash_light.instance()
	light.texture_scale = scale
	light.init_energy = energy
	light.lifetime = time
	light.position = Vector2(facing*16, 32) + offset
	self.add_child(light)



# Invincibility feature. When the player takes damage, the player becomes invincible for
# invincibility_time amount of seconds, set on the export.
func take_damage(damage_amount: int, source_direction = Vector2(), knockback_force:float = 0.0) -> void:
	if not is_invincible() and not get_state() == DYING_STATE:
		.take_damage(damage_amount, source_direction, knockback_force)
		
		$InvincibilityTimer.start()
		$Finder.get("hud").update_display()
		
		if self.is_dead():
			change_state(DYING_STATE)
		else:
			var main = $Finder.get("main")
			main.freeze(0.1)
			$Tween.interpolate_property($Sprite.get_material(), "shader_param/color",
										Color(1,1,1,1), Color(0,0,0,1), 0.7,
										Tween.TRANS_CIRC, Tween.EASE_IN)
			$Tween.start()
			$Sprite.release_damage_particles()
			$Sprite.modulate= Color(1,1,1,0.5)
			if get_state() == DIVING_STATE:
				change_state(BASE_STATE)
			
			# Send the player up if they fall on a bullet without jumping on it.
			if source_direction.y <= -0.7 and knockback_force == 0.0:
				self.velocity = Vector2(0, -base_jump_force/sqrt(1.5))
				can_dodge = true
				$Sprite.bounce_particles()

func is_invincible() -> bool:
	return not $InvincibilityTimer.is_stopped()



# Overriding this function to stop it from messing with dodge i-frames.
func on_platform_exited(body):
	if body.is_in_group("platform") and not get_state() == DODGING_STATE:
		self.set_collision_layer_bit(2, true)
		self.set_collision_mask_bit(2, true)



# These two functions use the larger, second wall detectors. Used on the wall jump feature.
func is_close_to_left_wall() -> bool:
	var i = 0
	for body in $Detectors/LeftWall.get_overlapping_bodies():
		if body.is_in_group("wall"):
			i += 1
	if i > 0:
		return true
	else:
		return false

func is_close_to_right_wall() -> bool:
	var i = 0
	for body in $Detectors/RightWall.get_overlapping_bodies():
		if body.is_in_group("wall"):
			i += 1
	if i > 0:
		return true
	else:
		return false

func is_close_to_a_wall() -> bool:
	return not is_close_to_left_wall() and not is_close_to_right_wall()



# These funcions handle jumping off of enemy bullets. Used on the base state.
func has_bounceables_underneath() -> bool:
	for area in get_node("Detectors/Bounceables").get_overlapping_areas():
		if area.is_in_group("bounceable"):
			return true
	return false

func get_bounceables():
	if not has_bounceables_underneath():
		return null
	else:
		var bounceables = []
		for area in get_node("Detectors/Bounceables").get_overlapping_areas():
			if area.is_in_group("bounceable"):
				bounceables.append(area.get_parent())
		return bounceables



# These methods deal with spikes.
func deal_with_spikes() -> void:
	if is_touching_spikes() and get_state() != DODGING_STATE and get_state() != DYING_STATE:
		if is_touching_down_spikes():
			self.take_damage(1, Vector2(0,-1), base_jump_force)
		elif is_touching_left_spikes():
			self.take_damage(1, Vector2(1,-1), base_jump_force*sqrt(2))
		elif is_touching_right_spikes():
			self.take_damage(1, Vector2(-1,-1), base_jump_force*sqrt(2))
		elif is_touching_up_spikes():
			self.take_damage(1, Vector2(0,1), 0)



# This function checks if the first value is between the next two.
static func interval(x: float, a: float, b: float) -> bool:
	if a < b:
		return a <= x and x <= b
	else:
		return b <= x and x <= a



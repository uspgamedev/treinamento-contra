extends AnimationControl

const LEFT_FLOOR: Vector2 = Vector2(-1,1)
const RIGHT_FLOOR: Vector2 = Vector2(1,1)
const NONE: Vector2 = Vector2.ZERO

onready var player: Node = get_parent()
onready var state: int = player.BASE_STATE
onready var sprite: Node = player.get_node("Sprite")
onready var jet_particles: Node = sprite.get_node("JetParticles")

func _process(delta):
	jet_particles.speed_scale = player.control
	
	state = player.get_state()
	
	if player.facing > 0:
		sprite.flip_h = false
		jet_particles.position.x = -18
	else:
		sprite.flip_h = true
		jet_particles.position.x = 18
	
	if not player.is_bouncing:
		match state:
			player.BASE_STATE:
				
				if player.is_on_floor():
					
					if player.move == 0:
						var ledge = get_ledges()
						match ledge:
							NONE:
								play_animation("Idle")
							LEFT_FLOOR:
								if player.facing > 0:
									play_animation("BalancingOnLeftLedge")
								else:
									play_animation("BalancingOnRightLedge")
							RIGHT_FLOOR:
								if player.facing > 0:
									play_animation("BalancingOnRightLedge")
								else:
									play_animation("BalancingOnLeftLedge")
					else:
						play_animation("Moving")
					
				elif player.is_airborne():
					play_animation("Airborne")
				
			player.WALL_CLINGING_STATE:
				play_animation("WallClinging")
			player.LEDGE_GRABBING_STATE:
				play_animation("LedgeGrabbing")
			player.LEDGE_CLIMBING_STATE:
				play_animation("LedgeClimbing")
			player.LEDGE_DROPPING_STATE:
				play_animation("LedgeDropping")
			player.GLIDING_STATE:
				play_animation("Gliding")
			player.DIVING_STATE:
				play_animation("Diving")
			player.DODGING_STATE:
				play_animation("Dodging")
			player.DYING_STATE:
				play_animation("Dying")




func play_animation(anim: String) -> void:
	if current_animation != anim:
		play(anim)



func get_ledges() -> Vector2:
	var ledge
	if player.is_on_left_floor_ledge():
		ledge = LEFT_FLOOR
		if distance(ledge):
			return ledge
		else:
			return NONE
	elif player.is_on_right_floor_ledge():
		ledge = RIGHT_FLOOR
		if distance(ledge):
			return ledge
		else:
			return NONE
	else:
		return NONE

func distance(ledge) -> bool:
	return player.get_ledge_distance(ledge) > 16



func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Bouncing":
		player.is_bouncing = false

func _on_AnimationPlayer_animation_changed(old_name, new_name):
	if old_name == "Bouncing" and new_name != "Bouncing":
		player.is_bouncing = false

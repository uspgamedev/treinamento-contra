extends AnimationControl

func _on_GunAnimations_animation_finished(anim_name):
	if anim_name == "ReloadingShotgun":
		self.play("Shotgun")

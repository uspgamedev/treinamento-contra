extends Node

###### LOAD STUFF ##########################################################

export (PackedScene) var first_stage
export (PackedScene) var game_over_screen
export (PackedScene) var title_screen
export (PackedScene) var hud
export (PackedScene) var screen_transition
export (bool) var no_shake
export (bool) var no_particles
export (bool) var no_coyote
export (bool) var no_bunny_hop

func _ready():
	$Screen.add_child(title_screen.instance())

func load_stage() -> void:
	var transition = screen_transition.instance()
	add_child(transition)
	$Tween.interpolate_property(transition.get_node("ScreenColor"), "color",
								Color(0,0,0,0), Color(0,0,0,1), 0.6,
								Tween.TRANS_QUAD, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	$Screen.get_children()[0].queue_free()
	$Screen.add_child(hud.instance())
	self.add_child(first_stage.instance())
	$Tween.interpolate_property(transition.get_node("ScreenColor"), "color",
								Color(0,0,0,1), Color(0,0,0,0), 1.4,
								Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	transition.queue_free()

func load_game_over_screen() -> void:
	var transition = screen_transition.instance()
	add_child(transition)
	$Tween.interpolate_property(transition.get_node("ScreenColor"), "color",
								Color(1,1,1,0), Color(1,1,1,1), 1.5,
								Tween.TRANS_CIRC, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	get_tree().get_nodes_in_group("stage")[0].queue_free()
	get_tree().get_nodes_in_group("hud")[0].queue_free()
	$Screen.add_child(game_over_screen.instance())
	$Tween.interpolate_property(transition.get_node("ScreenColor"), "color",
								Color(1,1,1,1), Color(0,0,0,0), 1.0,
								Tween.TRANS_QUAD, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	transition.queue_free()



############################################################################



##### GAMEPLAY PARTS #######################################################

const RIFLE: int = 0
const MACHINE_GUN: int = 1
const SHOTGUN: int = 2

onready var initial_gun = RIFLE

export (float, 0.001, 5) var control = 1.0



# Screen freeze function. Pauses the game for duration time in seconds, or pauses
# the game forever if you set a less than or equal to zero duration.
func freeze(duration: float) -> void:
	if duration > 0:
		$FreezeTimer.start(duration)
	get_tree().paused = true

# Called as signal when $FreezeTimer times out.
func freeze_ended():
	get_tree().paused = false



# Control settings for mouse and keyboard.
onready var mouse_and_keyboard: int = 1
onready var controller: int = 1

func change_mouse_and_keyboard(new_scheme) -> void:
	var mouse = InputEventMouseButton.new()
	var key = InputEventKey.new()
	
	##### Removing Scheme 1 ################################################
	if mouse_and_keyboard == 1:
		
		key.scancode = KEY_SPACE
		InputMap.action_erase_event("ui_jump", key)
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_erase_event("ui_dodge", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_erase_event("ui_dodge", key)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_erase_event("ui_shoot", mouse)
	########################################################################
		
	##### Removing Scheme 2 ################################################
	elif mouse_and_keyboard == 2:
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_erase_event("ui_jump", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_erase_event("ui_jump", key)
		
		key.scancode = KEY_SPACE
		InputMap.action_erase_event("ui_dodge", key)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_erase_event("ui_shoot", mouse)
	########################################################################
		
	##### Removing Scheme 3 ################################################
	elif mouse_and_keyboard == 3:
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_erase_event("ui_jump", mouse)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_erase_event("ui_dodge", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_erase_event("ui_dodge", key)
		
		key.scancode = KEY_SPACE
		InputMap.action_erase_event("ui_shoot", key)
	########################################################################
		
	
	mouse_and_keyboard = new_scheme
	
	##### Implementing Scheme 1 ############################################
	if mouse_and_keyboard == 1:
		
		key.scancode = KEY_SPACE
		InputMap.action_add_event("ui_jump", key)
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_add_event("ui_dodge", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_add_event("ui_dodge", key)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_add_event("ui_shoot", mouse)
	########################################################################
		
	##### Implementing Scheme 2 ############################################
	elif mouse_and_keyboard == 2:
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_add_event("ui_jump", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_add_event("ui_jump", key)
		
		key.scancode = KEY_SPACE
		InputMap.action_add_event("ui_dodge", key)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_add_event("ui_shoot", mouse)
	########################################################################
		
	##### Implementing Scheme 3 ############################################
	elif mouse_and_keyboard == 3:
		
		mouse.button_index = BUTTON_RIGHT
		InputMap.action_add_event("ui_jump", mouse)
		
		mouse.button_index = BUTTON_LEFT
		InputMap.action_add_event("ui_dodge", mouse)
		
		key.scancode = KEY_SHIFT
		InputMap.action_add_event("ui_dodge", key)
		
		key.scancode = KEY_SPACE
		InputMap.action_add_event("ui_shoot", key)
	########################################################################

############################################################################





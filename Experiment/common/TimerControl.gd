extends Timer


onready var base_wait_time = wait_time
onready var control: float = 1.0
onready var main: Node = get_tree().get_nodes_in_group("main")[0]


func _physics_process(delta):
	if not control == main.control:
		self.start(time_left/(main.control/control))
		control = main.control
		set_wait_time(base_wait_time)
	if base_wait_time != wait_time:
		base_wait_time = wait_time


func start(time_sec: float = -1.0) -> void:
	if time_sec > 0:
		set_wait_time(time_sec/control)
	else:
		set_wait_time(wait_time/control)
	.start()
	set_wait_time(base_wait_time)
extends Light2D

onready var main: Node = get_tree().get_nodes_in_group("main")[0]

var init_energy: float
var lifetime: float

func _ready():
	$Tween.interpolate_property(self, "energy", 1.0, init_energy, lifetime, Tween.TRANS_QUART, Tween.EASE_IN)
	$Tween.start()

func _physics_process(_delta):
	$Tween.playback_speed = main.control

func on_tween_completed():
	self.queue_free()

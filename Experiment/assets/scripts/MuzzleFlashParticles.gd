extends CPUParticles2D

onready var main: Node = get_tree().get_nodes_in_group("main")[0]

func _ready():
	self.emitting = true
	self.one_shot = true


func _process(_delta):
	self.speed_scale = main.control
	if not emitting:
		queue_free()
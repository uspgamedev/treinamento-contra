extends ParticlesControl

class_name OneShotParticles


func _ready():
	self.emitting = true
	self.one_shot = true


func _process(_delta):
	self.speed_scale = main.control
	if not emitting:
		queue_free()